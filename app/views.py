from app import app
from flask import render_template,flash,request,redirect,url_for,session,abort

from app.models import avanevaluation,user,companyregistration,Financial_Data
from app import db
import string
from urllib.parse import urlparse,urljoin
import random
import smtplib
from email.mime.multipart import MIMEMultipart
from sqlalchemy import or_,and_
from email.mime.text import MIMEText
import  pdb,json
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import os
from flask import make_response
from functools import wraps, update_wrapper


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):

        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        #response.headers['Cache-Control'] = 'no-store, no-cache'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)


def file_read(fname):
    with open(fname) as f:
        content_list = f.readlines()
        return content_list


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def emailsending(sendto,activationlink):
    froms = "santosh.erpincloud@gmail.com"
    smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Activation Link"
    msg['From'] = froms
    msg['To'] = sendto
    html = """\

         <html>

          <head></head>

           <body>

             <p>Hi!<br>

               thanks for registering for our services <br>
               click the below link to complete the registration successfully<br>""" + activationlink + """

            </p>

            </body>

           </html>

          """

    part2 = MIMEText(html, 'html')
    gmail_pwd = "$0lutions"
    msg.attach(part2)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(froms, gmail_pwd)
    smtpserver.sendmail(froms, sendto, msg.as_string())
    smtpserver.close()

@nocache
@app.route('/')
@app.route('/login/<uname>' ,methods=["GET","POST"])
@app.route('/login' ,methods=["GET","POST"])
def login(uname=None):

    now = datetime.datetime.now()
    print(now.year);
    yrlist = []
    for each in range(1, 4):
        yrlist.append(now.year - each)
    ##pdb.set_trace()

    if uname==None:
        if  not  session.get('username',''):
            return render_template("AvanEvalLoginPage.html",yrdisplay=yrlist)
        else:
            user_data = user.query.filter(or_(user.username == session['username'], user.useremail == session['username'])).first()
            if user_data.status == "1":
                print(user_data.username)
                return render_template("companydata.html", user12=user_data.username, usernm=user_data.useremail,
                                       yrdisplay=yrlist)
            if user_data.status == "2":
                print(user_data.username)

                return redirect(url_for("EvalCompanyDashboard", usernm=session['username'],user12=user_data.username,yrdisplay=yrlist))

#            return redirect(url_for("EvalCompanyDashboard", usernm=session['username'],user12=user_data.username,yrdisplay=yrlist))
    else:


     if uname!=session.get('username',''):

      userid = request.values.get('usernm','')
      userpw = request.values.get('psw','')
      if userid=='':
        return render_template("AvanEvalLoginPage.html",yrdisplay=yrlist)
      loginusername=user.query.filter(or_(user.username==userid,user.useremail==userid)).first()
      if not loginusername:
        message = "user is not created"
        return render_template("AvanEvalLoginPage.html", msg=message,yrdisplay=yrlist)
      else:
        user_data = user.query.filter(or_(user.username==userid,user.useremail==userid)).first()
        if user_data :
          if  check_password_hash(user_data.password_hash,userpw)==True:

             session['logged_in'] = True
             session['username'] = user_data.useremail
             session['message'] ="You are logged in"
             if user_data.status== "1":
                 return render_template("companydata.html", user=user_data.username, usernm=user_data.useremail,
                                        yrdisplay=yrlist)

             if user_data.status == "2":
                 return redirect(url_for("EvalCompanyDashboard", usernm=session['username'], yrdisplay=yrlist))


             else:
                  message = "Your account is not activated"
                  return render_template("AvanEvalLoginPage.html", msg=message,yrdisplay=yrlist)
          else:
              message = "username and password is worng"
              return render_template("AvanEvalLoginPage.html", msg=message,yrdisplay=yrlist)

     else:
        userid = request.values.get('usernm', '')
        userpw = request.values.get('psw', '')

        if not session.get('username', ''):
            return render_template("AvanEvalLoginPage.html",yrdisplay=yrlist)
        else:
         user_data = user.query.filter(or_(user.username == userid, user.useremail == userid)).first()
         if user_data.status == "1":
             return render_template("companydata.html", user12=user_data.username, usernm=user_data.useremail,
                                    yrdisplay=yrlist)


         if user_data.status == "2":
             print(user_data.username)
             return redirect(url_for("EvalCompanyDashboard", user12=user_data.username, usernm=session['username'], yrdisplay=yrlist))



@nocache
@app.route('/joinus_userdetails',methods=["GET","POST"] )
def joinus_userdetails():

    generate_varification_code=id_generator()
    pswd=request.values['psw']
    cpswd=request.values['cnfpsw']
    emailtemp=request.values['userid']

    currentpath = os.getcwd()
    print(currentpath)
    currentpath=currentpath.replace('\\', '//')
    print(currentpath)
    filepath=currentpath+"//Excel sheet//AvoidEmail.txt"
    sampleemail1=file_read(filepath)


    emailtemp1=emailtemp.split('@')[1]
    emaildtemp2=emailtemp1+'\n'
    if emaildtemp2 in sampleemail1 :
            message = "invalid email id"
            return render_template("AvanEvalLoginPage.html", msg=message)
    if(pswd == cpswd ):
      print (generate_varification_code)
      user_data = user.query.filter(
          or_(user.username == request.values['userid'], user.useremail == request.values['userid'])).first()
      if user_data:
          message = "Already Registered"
          return render_template("AvanEvalLoginPage.html", msg=message)
      else:

       user1 = user(request.values['uname'],
                 request.values['userid'],
                 request.values['psw'],
                   generate_varification_code,
                   "0");
       db.session.add(user1)
       db.session.commit()
       print("successful_______________________________________________")
       activationlink="http://localhost:5000/verify/"+request.values['uname']+"/"+generate_varification_code
       to = request.values['userid']
       emailsending(to,activationlink)
       message = "please check your mail box for activation link"
       return render_template("AvanEvalLoginPage.html", msg=message)
    else:
        flash("password and confirm password is not matching")
        message = "password and confirm password is not matching"
        return render_template("AvanEvalLoginPage.html", msg=message)


@nocache
@app.route('/verify/<emailid>/<verificationcode>',methods=["GET","POST"])
def verify(emailid,verificationcode):

    next = request.args.get("next")
    if not is_safe_url(next):

        return abort(404)
    else:
        user_data = user.query.filter(and_(or_(user.username==emailid,user.useremail==emailid),user.varificationcode==verificationcode)).first()
        if not user_data:
            message = "Invalid Link"
            return render_template("AvanEvalLoginPage.html", msg=message)

        elif user_data.status==1:
           message = "Invalid Link"
           return render_template("AvanEvalLoginPage.html", msg=message)
        else:
           user_data.status = 1
           db.session.commit()
           message = "your varification successful you can login now"
           return render_template("AvanEvalLoginPage.html", msg=message)


@nocache
@login_required
@app.route("/firstregistration", methods=["GET", "POST"])
def registration():
    return redirect(url_for("EvalCompanyDashboard", usernm=session['username']))



@login_required
@nocache
@app.route('/EvalCompanyDashboard' ,methods=['GET', 'POST'])
@app.route('/EvalCompanyDashboard/<usernm>' ,methods=['GET', 'POST'])
def EvalCompanyDashboard(usernm=None ):
    ##pdb.set_trace()
    now = datetime.datetime.now()
    print(now.year);
    yrlist = []
    for each in range(1, 4):
        yrlist.append(now.year - each)
    print(yrlist)
    csession = session
    print(csession)

    if session.get('username', ''):
        company_data = Financial_Data.query.filter_by(emailaddress=session.get('username', '')).all()
        user_data = user.query.filter(or_(user.username == session.get('username', ''), user.useremail == session.get('username', ''))).first()

        return render_template("CompanyDashBoard.html", usernm=session['username'], user12=user_data.username, yrdisplay=yrlist,
                               current_session=csession,userdata=company_data)
    else:
        if  usernm == session.get('username',''):
     #db_data_filter_by_username = avanevaluation.query.filter_by(cuser=usernm).all()
    #E avanlen=len(db_data_filter_by_username)
     #print (avanlen)

              company_data = Financial_Data.query.filter_by(emailaddress=usernm).all()
              user_data = user.query.filter(
        or_(user.username == session.get('username', ''), user.useremail == session.get('username', ''))).first()

              if company_data:


                     return render_template("CompanyDashBoard.html", usernm=session['username'],yrdisplay=yrlist,userdata=company_data, user12=user_data.username)
              else:
                      return render_template("companydata.html", usernm=session['username'],yrdisplay=yrlist, user12=user_data.username)
        else:
            return redirect(url_for('login'))





@nocache
@login_required
@app.route('/EvalCompanyDashboard/datasubmit/<user123>',methods=['GET', 'POST'])
def datasubmit(user123=None):
  if user123 == session.get('username',''):
    financial_data = Financial_Data.query.filter_by(emailaddress=request.values['nowuser']).first()
    if not financial_data:
      companyregistration1 = companyregistration(
      companyname = request.values["companyname"],
      address = request.values['address'],
      city = request.values['city'],
      state = request.values['state'],
      zip = request.values['zip'],
      country = request.values['country'],
      website = request.values['website'],
      firstname = request.values['firstname'],
      lastname = request.values['lastname'],
      contactnumber = request.values['contactnumber'],
      emailaddress = request.values['emailaddress']
       )
      db.session.add(companyregistration1)
      db.session.commit()
      print("successful")
    eval_data = Financial_Data.query.filter_by(year=request.values['years'][-4:],emailaddress=request.values['nowuser']).first()
    if eval_data :
     if  request.values['revenue']>"0" and request.values['revenue']!="NaN":
      eval_data.revenues = request.values['revenue']
     if request.values['materialexpenses']>"0" and request.values['materialexpenses']!="NaN":
      eval_data.materialexpenses = request.values['materialexpenses']
     if request.values['personnelexpenses'] >"0" and request.values['personnelexpenses']!="NaN":
      eval_data.personnelexpenses = request.values['personnelexpenses']
     if request.values['otherexpenses'] >"0" and request.values['otherexpenses']!="NaN":
      eval_data.otherexpenses = request.values['otherexpenses']
     if request.values['otherincome'] >"0" and request.values['otherincome']!="NaN":
      eval_data.otherincome = request.values['otherincome']
     if request.values['ebitda'] >"0" and request.values['ebitda']!="NaN":
      eval_data.ebitda = request.values['ebitda']
     if request.values['ebitdamargin']  >"0" and request.values['ebitdamargin']!="NaN":
      eval_data.ebitdamargin = request.values['ebitdamargin']
     if request.values['depreciation'] >"0" and request.values['depreciation']!="NaN":
      eval_data.depreciation = request.values['depreciation']
     if request.values['ebit'] >"0" and request.values['ebit']!="NaN":
      eval_data.ebit = request.values['ebit']
     if request.values['ebitmargin']>"0" and request.values['ebitmargin']!="NaN":
      eval_data.ebitmargin = request.values['ebitmargin']
     if request.values['interestexpenses'] >"0" and request.values['interestexpenses']!="NaN":
      eval_data.interestexpenses = request.values['interestexpenses']
     if request.values['interestincome'] >"0" and request.values['interestincome']!="NaN":
       eval_data.interestincome = request.values['interestincome']
     if request.values['ebt'] >"0" and request.values['ebt']!="NaN":
       eval_data.ebt = request.values['ebt']
     if request.values['incometax']>"0" and request.values['incometax']!="NaN":
       eval_data.incometax = request.values['incometax']
     if request.values['netincome']>"0" and request.values['netincome']!="NaN":
       eval_data.netincome = request.values['netincome']

     if  request.values['fixedasset']>"0" and request.values['fixedasset']!="NaN":
       eval_data.fixedasset= request.values['fixedasset']
     if request.values['finanacialasset'] >"0" and request.values['finanacialasset']!="NaN":
        eval_data.finanacialasset= request.values['finanacialasset']
     if  request.values['inventories']>"0" and request.values['inventories']!="NaN":
        eval_data.inventories= request.values['inventories']
     if request.values['trade'] >"0" and request.values['trade']!="NaN":
        eval_data.trade= request.values['trade']
     if request.values['othercurrentassets']>"0" and request.values['othercurrentassets']!="NaN":
        eval_data.othercurrentassets= request.values['othercurrentassets']
     if request.values['cash'] >"0" and request.values['cash']!="NaN":
        eval_data.cash= request.values['cash']
     if request.values['totalcurrentassets']>"0" and request.values['totalcurrentassets']!="NaN":
        eval_data.totalcurrentassets= request.values['totalcurrentassets']
     if request.values['totalassets']>"0" and request.values['totalassets']!="NaN":
        eval_data.totalassets= request.values['totalassets']
     if request.values['equity']>"0" and request.values['equity']!="NaN":
        eval_data.equity= request.values['equity']
     if request.values['financialdebt'] >"0" and request.values['financialdebt']!="NaN":
        eval_data.financialdebt= request.values['financialdebt']
     if request.values['pensionprovisions']>"0" and request.values['pensionprovisions']!="NaN":
        eval_data.pensionprovisions= request.values['pensionprovisions']
     if request.values['otherprovisions']>"0" and request.values['otherprovisions']!="NaN":
        eval_data.otherprovisions= request.values['otherprovisions']
     if request.values['tradepayables']>"0" and request.values['tradepayables']!="NaN":
        eval_data.tradepayables= request.values['tradepayables']
     if request.values['otherliabilities']>"0" and request.values['otherliabilities']!="NaN":
        eval_data.otherliabilities= request.values['otherliabilities']
     if request.values['totalliabilities']>"0" and request.values['totalliabilities']!="NaN":
        eval_data.totalliabilities= request.values['totalliabilities']
     if request.values['totalequity']>"0" and request.values['totalequity']!="NaN":
        eval_data.totalequity= request.values['totalequity']
     if request.values['capitalexpendituresinfixedasset']>"0" and request.values['capitalexpendituresinfixedasset']!="NaN":
        eval_data.capitalexpendituresinfixedasset= request.values['capitalexpendituresinfixedasset']
     if request.values['capitalexpenditurepercent']>"0" and request.values['capitalexpenditurepercent']!="NaN":
        eval_data.capitalexpenditurepercent= request.values['capitalexpenditurepercent']
     if request.values['netcurrentassets']>"0" and request.values['netcurrentassets']!="NaN":
        eval_data.netcurrentassets= request.values['netcurrentassets']
     if request.values['netcurrentassetspercent']>"0" and request.values['netcurrentassetspercent']!="NaN":
        eval_data.netcurrentassetspercent= request.values['netcurrentassetspercent']

     db.session.commit()
    else:

        Financial_Data1 = Financial_Data(year= request.values['years'][-4:],
                                         companyname=request.values["companyname"],
                                         emailaddress=request.values['emailaddress'],
                                         revenues = request.values['revenue'],
                                         materialexpenses = request.values['materialexpenses'],
                                         personnelexpenses = request.values['personnelexpenses'],
                                         otherexpenses = request.values['otherexpenses'],
                                         otherincome = request.values['otherincome'],
                                         ebitda = request.values['ebitda'],
                                         ebitdamargin = request.values['ebitdamargin'],
                                         depreciation=request.values['depreciation'],
                                         ebit=request.values['ebit'],
                                         ebitmargin = request.values['ebitmargin'],
                                         interestexpenses=request.values['interestexpenses'],
                                         interestincome = request.values['interestincome'],
                                         ebt = request.values['ebt'],
                                         incometax = request.values['incometax'],
                                         netincome = request.values['netincome'],
                                         fixedasset=request.values['fixedasset'],
                                         finanacialasset=request.values['finanacialasset'],
                                         inventories=request.values['inventories'],
                                         trade=request.values['trade'],
                                         othercurrentassets=request.values['othercurrentassets'],
                                         cash=request.values['cash'],
                                         totalcurrentassets=request.values['totalcurrentassets'],
                                         totalassets=request.values['totalassets'],
                                         equity=request.values['equity'],
                                         financialdebt=request.values['financialdebt'],
                                         pensionprovisions=request.values['pensionprovisions'],
                                         otherprovisions=request.values['otherprovisions'],
                                         tradepayables=request.values['tradepayables'],
                                         otherliabilities=request.values['otherliabilities'],
                                         totalliabilities=request.values['totalliabilities'],
                                         totalequity=request.values['totalequity'],
                                         capitalexpendituresinfixedasset=request.values['capitalexpendituresinfixedasset'],
                                         capitalexpenditurepercent=request.values['capitalexpenditurepercent'],
                                         netcurrentassets=request.values['netcurrentassets'],
                                         netcurrentassetspercent=request.values['netcurrentassetspercent'])

        db.session.add(Financial_Data1)
        print("successful")
    user_data_new = user.query.filter(or_(user.username == user123, user.useremail == user123)).first()
    if user_data_new:
        user_data_new.status = 2
    db.session.commit()
    return redirect(url_for("EvalCompanyDashboard", usernm=session['username']))
  else:
      return redirect(url_for('login'))





@login_required
@nocache
@app.route('/logout' ,methods=['GET', 'POST'])
def logout():
        session.pop('username',None)
        session.pop('logged_in', None)
        session['username'] =""
        session['message'] = "You were logged out"
        flash('You were logged out.')
        return redirect(url_for('login'))




@nocache
@login_required
@app.route('/autocalculation' ,methods=['GET', 'POST'])
def autocalculation():
     if session.get('username', ''):
       #  pdb.set_trace()
         cuser=session.get('username', '')
         print(session.get('username', ''))
         financedata = Financial_Data.query.filter_by(emailaddress=cuser).all()
         print(len(financedata))
         c0=float(financedata[0].revenues)
         nth=len(financedata)-1
         cn=float(financedata[nth].revenues)
    #old     revrate=(cn/c0)**(1/float(nth))
         revrate =((cn / c0) ** (1 / float(nth)))*100
         now = datetime.datetime.now()
         print(now.year);
         yrlist = []
         a=list(range(1, 4))
         a.reverse()
         for each in a:
             yrlist.append(now.year - each)
         yrlist.append(now.year)

         for each in range(1, 6):
             yrlist.append(now.year + each)
         print(yrlist)
         tr, me, pe, oe, oi, dep, ie, ii, it, oca, pp, op, ebt, perceofa=0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
         for each in range(0,len(financedata)):
                     tr = tr + int(financedata[each].revenues)
                     me = me +int( financedata[each].materialexpenses)
                     pe = pe + int(financedata[each].personnelexpenses)
                     oe = oe + int(financedata[each].otherexpenses)
                     oi = oi + int(financedata[each].otherincome)
                     dep = dep + int(financedata[each].depreciation)
                     ie = ie + int(financedata[each].interestexpenses)
                     ii = ii + int(financedata[each].interestincome)
                     it = it + int(financedata[each].incometax)
                     oca=oca+int(financedata[each].othercurrentassets)
                     pp = pp + int(financedata[each].personnelexpenses)
                     op = op + int(financedata[each].totalliabilities)
                     ebt=ebt+int(financedata[each].ebt)
                     perceofa=perceofa+float(financedata[each].capitalexpenditurepercent)

         merate=round((me/tr)*100,2)
         perate = round((pe / tr) * 100,2)
         oerate =round( (oe / tr) * 100,2)
         oirate = round((oi / tr) * 100,2)
         deprate =round( (dep / tr) * 100,2)
         ierate=round((ie/tr)*100,2)
         iirate = round((ii / tr) * 100,2)
    #old     itrate = (it / tr) * 100
         itrate = round((it / ebt),2)
         avgpp=round(pp/3,2)
         avgop=round(op/3,2)
         avgperceofa=round(perceofa/3,2)
         print("revenue percent"+str(revrate))
         print("me percent"+str(merate))
         print("pe percent"+str(perate))
         print("oe percent"+str(oerate))
         print("oi percent"+str(oirate))
         print("dep percent"+str(deprate))
         print("ie percent"+str(ierate))
         print("ii percent"+str(iirate))
         print("it percent"+str(itrate))
         print("it percent" + str(avgpp))
         print("it percent" + str(avgop))
         print(perceofa)
         print(perceofa/3)
         print(avgperceofa)


         user_data = user.query.filter(
             or_(user.username == session.get('username', ''), user.useremail == session.get('username', ''))).first()


         return render_template("P&LAuto-Calculation.html",yrdisplay=yrlist,userdata=financedata,pertrrate=revrate,permerate=merate,
                                user12=user_data.username,perperate=perate,peroerate=oerate,peroirate=oirate,perdeprate=deprate,perierate=ierate,periirate=iirate,peritrate=itrate,averageop=avgop,averagepp=avgpp,avgpercoefa=avgperceofa )

     else:
         return redirect(url_for('login'))
         #
#          ####pdb.set_trace()
#          companydata=companyregistration.query.filter_by(emailaddress=user).all()
#          print(companydata[0])
#          tr=0;
#          me=0
#          pe=0
#          oe=0
#          oi=0
#          dep=0
#          ie=0
#          ii=0
#          it=0
#
#          for each in range(0,len(companydata)):
#             tr=tr+int(companydata[each].revenues)
#             me = me +int( companydata[each].materialexpenses)
#             pe = pe + int(companydata[each].personnelexpenses)
#             oe = oe + int(companydata[each].otherexpenses)
#             oi = oi + int(companydata[each].otherincome)
#             dep = dep + int(companydata[each].depreciation)
#             ie = ie + int(companydata[each].interestexpenses)
#             ii = ii + int(companydata[each].interestincome)
#             it = it + int(companydata[each].incometax)
#          tr1=0
#          tr1=int( companydata[1].materialexpenses)-int( companydata[0].materialexpenses)
#          tr2 = 0
#          tr2 = int(companydata[2].materialexpenses) - int(companydata[1].materialexpenses)
#
#          tr=round(tr/3)
#          me=round(me/3)
#          oe=round(oe/3)
#          oi=round(oi/3)
#          dep=round(dep/3)
#          ie=round(ie/3)
#          ii=round(ii/3)
#          it=round(it/3)
#          return render_template("AutoIncrement.html",valtr=tr,valpe=pe,valme=me,valoe=oe,valoi=oi,valdep=dep,valie=ie,valii=ii,valit=it)





@nocache
@login_required
@app.route('/manualmode' ,methods=['GET', 'POST'])
def manualmode():
     #pdb.set_trace()
     if session.get('username', ''):
         ##pdb.set_trace()
         cuser=session.get('username', '')
         print(session.get('username', ''))
         financedata = Financial_Data.query.filter_by(emailaddress=cuser).all()
         avandata = avanevaluation.query.filter_by(emailaddress=cuser).all()
         print(len(financedata))
         now = datetime.datetime.now()
         print(now.year);
         yrlist = []
         a=list(range(1, 4))
         a.reverse()
         for each in a:
             yrlist.append(now.year - each)
         yrlist.append(now.year)

         for each in range(1, 6):
             yrlist.append(now.year + each)
         print(yrlist)
         user_data = user.query.filter(
             or_(user.username == session.get('username', ''), user.useremail == session.get('username', ''))).first()
         if len(avandata) > 0:
             print("Avan html--------------------------------------------")
             return render_template("P&LManual-Calculation.html", yrdisplay=yrlist, userdata=avandata,
                                    user12=user_data.username)
         print("finance html -----------------------------")
         return render_template("P&LManual-Calculation.html",yrdisplay=yrlist,userdata=financedata,user12=user_data.username)
     else:

         return redirect(url_for('login'))


@nocache
@login_required
@app.route('/forgotpassword', methods=['GET', 'POST'])
def forgotpassword():
    id=request.values["userid"]
    print(id)
    useravailable=user.query.filter_by(useremail=id).first()
    username=useravailable.username
    if useravailable:
        generate_varification_code = id_generator()
        print(generate_varification_code)
        useravailable.varificationcode =generate_varification_code
        db.session.commit()
        print("successful_______________________________________________")
        activationlink = "http://localhost:5000/verified/" + username + "/" + generate_varification_code+ "/change_password"
        to = request.values['userid']
        emailsending(to, activationlink)
        message = "please check your mail box for activation link"
        return render_template("AvanEvalLoginPage.html", msg=message)


    else:
        message="EMail id is not registered"
        return render_template("AvanEvalLoginPage.html", msg=message)


@nocache
@login_required
@app.route('/verified/<user1>/<varificationcode1>/change_password', methods=['GET', 'POST'])
def passwordchange(user1=None,varificationcode1=None):

    print(user1)
    print(varificationcode1)
    useravailable = user.query.filter_by(username=user1).first()
    realvarificationcdoe=useravailable.varificationcode
    if realvarificationcdoe==varificationcode1:
       currentuser=useravailable.username
       print("real link")
       return render_template("ChangePassword.html",usernm=currentuser)

    else:
        message="This link is unauthorised"
        return render_template("AvanEvalLoginPage.html", msg=message)



@nocache
@login_required
@app.route('/updatepassword/<user1>', methods=['GET', 'POST'])
def passwordUpdate(user1=None):
    useravailable = user.query.filter_by(username=user1).first()
    useravailable.password_hash = generate_password_hash(request.values["newpassword"])

    useravailable.varificationcode = ""
    db.session.commit()
    currentuser = useravailable.username
    message="Now you can login into your account with new password"
    return render_template("AvanEvalLoginPage.html", msg=message)


@app.route('/getsession' ,methods=['GET', 'POST'])
def getsession():

      sessionmessage=session.get('message','')
      return json.dumps({"session_message":sessionmessage  })

@app.route('/testing' ,methods=['GET', 'POST'])
def testing():
    message="hiiiiiii"
    return render_template("AvanEvalloginnewpage.html", msg=message)

@nocache
@login_required
@app.route('/EvalCompanyDashboard/dataupload/<user123>',methods=['GET', 'POST'])
def datasubmit1(user123=None):
  #pdb.set_trace()
  user_data_new1 = user.query.filter(or_(user.username == user123, user.useremail == user123)).first()
  userid=user_data_new1.useremail
  if userid == session.get('username',''):
    eval_data = avanevaluation.query.filter_by(year=request.values['years'][-4:],emailaddress=userid).first()
    if eval_data :
     if  request.values['revenue']>"0" and request.values['revenue']!="NaN":
      eval_data.revenues = request.values['revenue']
     if request.values['materialexpenses']>"0" and request.values['materialexpenses']!="NaN":
      eval_data.materialexpenses = request.values['materialexpenses']
     if request.values['personnelexpenses'] >"0" and request.values['personnelexpenses']!="NaN":
      eval_data.personnelexpenses = request.values['personnelexpenses']
     if request.values['otherexpenses'] >"0" and request.values['otherexpenses']!="NaN":
      eval_data.otherexpenses = request.values['otherexpenses']
     if request.values['otherincome'] >"0" and request.values['otherincome']!="NaN":
      eval_data.otherincome = request.values['otherincome']
     if request.values['ebitda'] >"0" and request.values['ebitda']!="NaN":
      eval_data.ebitda = request.values['ebitda']
     if request.values['ebitdamargin']  >"0" and request.values['ebitdamargin']!="NaN":
      eval_data.ebitdamargin = request.values['ebitdamargin']
     if request.values['depreciation'] >"0" and request.values['depreciation']!="NaN":
      eval_data.depreciation = request.values['depreciation']
     if request.values['ebit'] >"0" and request.values['ebit']!="NaN":
      eval_data.ebit = request.values['ebit']
     if request.values['ebitmargin']>"0" and request.values['ebitmargin']!="NaN":
      eval_data.ebitmargin = request.values['ebitmargin']
     if request.values['interestexpenses'] >"0" and request.values['interestexpenses']!="NaN":
      eval_data.interestexpenses = request.values['interestexpenses']
     if request.values['interestincome'] >"0" and request.values['interestincome']!="NaN":
       eval_data.interestincome = request.values['interestincome']
     if request.values['ebt'] >"0" and request.values['ebt']!="NaN":
       eval_data.ebt = request.values['ebt']
     if request.values['incometax']>"0" and request.values['incometax']!="NaN":
       eval_data.incometax = request.values['incometax']
     if request.values['netincome']>"0" and request.values['netincome']!="NaN":
       eval_data.netincome = request.values['netincome']

     if  request.values['fixedasset']>"0" and request.values['fixedasset']!="NaN":
       eval_data.fixedasset= request.values['fixedasset']
     if request.values['finanacialasset'] >"0" and request.values['finanacialasset']!="NaN":
        eval_data.finanacialasset= request.values['finanacialasset']
     if  request.values['inventories']>"0" and request.values['inventories']!="NaN":
        eval_data.inventories= request.values['inventories']
     if request.values['trade'] >"0" and request.values['trade']!="NaN":
        eval_data.trade= request.values['trade']
     if request.values['othercurrentassets']>"0" and request.values['othercurrentassets']!="NaN":
        eval_data.othercurrentassets= request.values['othercurrentassets']
     if request.values['cash'] >"0" and request.values['cash']!="NaN":
        eval_data.cash= request.values['cash']
     if request.values['totalcurrentassets']>"0" and request.values['totalcurrentassets']!="NaN":
        eval_data.totalcurrentassets= request.values['totalcurrentassets']
     if request.values['totalassets']>"0" and request.values['totalassets']!="NaN":
        eval_data.totalassets= request.values['totalassets']
     if request.values['equity']>"0" and request.values['equity']!="NaN":
        eval_data.equity= request.values['equity']
     if request.values['financialdebt'] >"0" and request.values['financialdebt']!="NaN":
        eval_data.financialdebt= request.values['financialdebt']
     if request.values['pensionprovisions']>"0" and request.values['pensionprovisions']!="NaN":
        eval_data.pensionprovisions= request.values['pensionprovisions']
     if request.values['otherprovisions']>"0" and request.values['otherprovisions']!="NaN":
        eval_data.otherprovisions= request.values['otherprovisions']
     if request.values['tradepayables']>"0" and request.values['tradepayables']!="NaN":
        eval_data.tradepayables= request.values['tradepayables']
     if request.values['otherliabilities']>"0" and request.values['otherliabilities']!="NaN":
        eval_data.otherliabilities= request.values['otherliabilities']
     if request.values['totalliabilities']>"0" and request.values['totalliabilities']!="NaN":
        eval_data.totalliabilities= request.values['totalliabilities']
     if request.values['totalequity']>"0" and request.values['totalequity']!="NaN":
        eval_data.totalequity= request.values['totalequity']
     if request.values['capitalexpendituresinfixedasset']>"0" and request.values['capitalexpendituresinfixedasset']!="NaN":
        eval_data.capitalexpendituresinfixedasset= request.values['capitalexpendituresinfixedasset']
     if request.values['capitalexpenditurepercent']>"0" and request.values['capitalexpenditurepercent']!="NaN":
        eval_data.capitalexpenditurepercent= request.values['capitalexpenditurepercent']
     if request.values['netcurrentassets']>"0" and request.values['netcurrentassets']!="NaN":
        eval_data.netcurrentassets= request.values['netcurrentassets']
     if request.values['netcurrentassetspercent']>"0" and request.values['netcurrentassetspercent']!="NaN":
        eval_data.netcurrentassetspercent= request.values['netcurrentassetspercent']

     db.session.commit()
    else:

        avanevaluation1 = avanevaluation(year= request.values['years'][-4:],
                                         emailaddress=userid,
                                         revenues = request.values['revenue'],
                                         materialexpenses = request.values['materialexpenses'],
                                         personnelexpenses = request.values['personnelexpenses'],
                                         otherexpenses = request.values['otherexpenses'],
                                         otherincome = request.values['otherincome'],
                                         ebitda = request.values['ebitda'],
                                         ebitdamargin = request.values['ebitdamargin'],
                                         depreciation=request.values['depreciation'],
                                         ebit=request.values['ebit'],
                                         ebitmargin = request.values['ebitmargin'],
                                         interestexpenses=request.values['interestexpenses'],
                                         interestincome = request.values['interestincome'],
                                         ebt = request.values['ebt'],
                                         incometax = request.values['incometax'],
                                         netincome = request.values['netincome'],
                                         fixedasset=request.values['fixedasset'],
                                         finanacialasset=request.values['finanacialasset'],
                                         inventories=request.values['inventories'],
                                         trade=request.values['trade'],
                                         othercurrentassets=request.values['othercurrentassets'],
                                         cash=request.values['cash'],
                                         totalcurrentassets=request.values['totalcurrentassets'],
                                         totalassets=request.values['totalassets'],
                                         equity=request.values['equity'],
                                         financialdebt=request.values['financialdebt'],
                                         pensionprovisions=request.values['pensionprovisions'],
                                         otherprovisions=request.values['otherprovisions'],
                                         tradepayables=request.values['tradepayables'],
                                         otherliabilities=request.values['otherliabilities'],
                                         totalliabilities=request.values['totalliabilities'],
                                         totalequity=request.values['totalequity'],
                                         capitalexpendituresinfixedasset=request.values['capitalexpendituresinfixedasset'],
                                         capitalexpenditurepercent=request.values['capitalexpenditurepercent'],
                                         netcurrentassets=request.values['netcurrentassets'],
                                         netcurrentassetspercent=request.values['netcurrentassetspercent'])

        db.session.add(avanevaluation1)
        print("successful")
    db.session.commit()
  else:
      return redirect(url_for('login'))






@nocache
@login_required
@app.route('/datasubmit/<user123>',methods=['GET', 'POST'])
def datasubmitfinance(user123=None):
  #pdb.set_trace()
  user_data_new1 = user.query.filter(or_(user.username == user123, user.useremail == user123)).first()
  userid=user_data_new1.useremail
  if userid == session.get('username',''):
    eval_data = Financial_Data.query.filter_by(year=request.values['years'][-4:],emailaddress=userid).first()
    if eval_data :
     if  request.values['revenue']>"0" and request.values['revenue']!="NaN":
      eval_data.revenues = request.values['revenue']
     if request.values['materialexpenses']>"0" and request.values['materialexpenses']!="NaN":
      eval_data.materialexpenses = request.values['materialexpenses']
     if request.values['personnelexpenses'] >"0" and request.values['personnelexpenses']!="NaN":
      eval_data.personnelexpenses = request.values['personnelexpenses']
     if request.values['otherexpenses'] >"0" and request.values['otherexpenses']!="NaN":
      eval_data.otherexpenses = request.values['otherexpenses']
     if request.values['otherincome'] >"0" and request.values['otherincome']!="NaN":
      eval_data.otherincome = request.values['otherincome']
     if request.values['ebitda'] >"0" and request.values['ebitda']!="NaN":
      eval_data.ebitda = request.values['ebitda']
     if request.values['ebitdamargin']  >"0" and request.values['ebitdamargin']!="NaN":
      eval_data.ebitdamargin = request.values['ebitdamargin']
     if request.values['depreciation'] >"0" and request.values['depreciation']!="NaN":
      eval_data.depreciation = request.values['depreciation']
     if request.values['ebit'] >"0" and request.values['ebit']!="NaN":
      eval_data.ebit = request.values['ebit']
     if request.values['ebitmargin']>"0" and request.values['ebitmargin']!="NaN":
      eval_data.ebitmargin = request.values['ebitmargin']
     if request.values['interestexpenses'] >"0" and request.values['interestexpenses']!="NaN":
      eval_data.interestexpenses = request.values['interestexpenses']
     if request.values['interestincome'] >"0" and request.values['interestincome']!="NaN":
       eval_data.interestincome = request.values['interestincome']
     if request.values['ebt'] >"0" and request.values['ebt']!="NaN":
       eval_data.ebt = request.values['ebt']
     if request.values['incometax']>"0" and request.values['incometax']!="NaN":
       eval_data.incometax = request.values['incometax']
     if request.values['netincome']>"0" and request.values['netincome']!="NaN":
       eval_data.netincome = request.values['netincome']

     if  request.values['fixedasset']>"0" and request.values['fixedasset']!="NaN":
       eval_data.fixedasset= request.values['fixedasset']
     if request.values['finanacialasset'] >"0" and request.values['finanacialasset']!="NaN":
        eval_data.finanacialasset= request.values['finanacialasset']
     if  request.values['inventories']>"0" and request.values['inventories']!="NaN":
        eval_data.inventories= request.values['inventories']
     if request.values['trade'] >"0" and request.values['trade']!="NaN":
        eval_data.trade= request.values['trade']
     if request.values['othercurrentassets']>"0" and request.values['othercurrentassets']!="NaN":
        eval_data.othercurrentassets= request.values['othercurrentassets']
     if request.values['cash'] >"0" and request.values['cash']!="NaN":
        eval_data.cash= request.values['cash']
     if request.values['totalcurrentassets']>"0" and request.values['totalcurrentassets']!="NaN":
        eval_data.totalcurrentassets= request.values['totalcurrentassets']
     if request.values['totalassets']>"0" and request.values['totalassets']!="NaN":
        eval_data.totalassets= request.values['totalassets']
     if request.values['equity']>"0" and request.values['equity']!="NaN":
        eval_data.equity= request.values['equity']
     if request.values['financialdebt'] >"0" and request.values['financialdebt']!="NaN":
        eval_data.financialdebt= request.values['financialdebt']
     if request.values['pensionprovisions']>"0" and request.values['pensionprovisions']!="NaN":
        eval_data.pensionprovisions= request.values['pensionprovisions']
     if request.values['otherprovisions']>"0" and request.values['otherprovisions']!="NaN":
        eval_data.otherprovisions= request.values['otherprovisions']
     if request.values['tradepayables']>"0" and request.values['tradepayables']!="NaN":
        eval_data.tradepayables= request.values['tradepayables']
     if request.values['otherliabilities']>"0" and request.values['otherliabilities']!="NaN":
        eval_data.otherliabilities= request.values['otherliabilities']
     if request.values['totalliabilities']>"0" and request.values['totalliabilities']!="NaN":
        eval_data.totalliabilities= request.values['totalliabilities']
     if request.values['totalequity']>"0" and request.values['totalequity']!="NaN":
        eval_data.totalequity= request.values['totalequity']
     if request.values['capitalexpendituresinfixedasset']>"0" and request.values['capitalexpendituresinfixedasset']!="NaN":
        eval_data.capitalexpendituresinfixedasset= request.values['capitalexpendituresinfixedasset']
     if request.values['capitalexpenditurepercent']>"0" and request.values['capitalexpenditurepercent']!="NaN":
        eval_data.capitalexpenditurepercent= request.values['capitalexpenditurepercent']
     if request.values['netcurrentassets']>"0" and request.values['netcurrentassets']!="NaN":
        eval_data.netcurrentassets= request.values['netcurrentassets']
     if request.values['netcurrentassetspercent']>"0" and request.values['netcurrentassetspercent']!="NaN":
        eval_data.netcurrentassetspercent= request.values['netcurrentassetspercent']

     db.session.commit()
  else:
      return redirect(url_for('login'))



