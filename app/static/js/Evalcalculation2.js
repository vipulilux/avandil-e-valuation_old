    $(document).ready(function(){

       $(".txt,.txt1,.txt2").keyup(function(){
                   $(this).val(german_format(normal_format($(this).val())));

        });

       $(".txt,.txt1,.txt2").change(function() {
         var incometaxs=0,interestincomes=0,interestexp=0,ebitval=0,rev=0,matexp=0,otherincomes=0,persexp=0,otherexp=0,otherincomes=0,ebitdaval=0,depreciationval=0,depreciationval=0;
         var incometaxsperc=0,interestincomesperc=0,interestexpperc=0,ebitperc=0,revperc=0,matexpperc=0,persexpperc=0,otherexpperc=0,otherincomesperc=0,otherincomesperc=0,ebitdaperc=0,depreciationperc=0,depreciationperc=0;

	      if(!isNaN(this.value)&& this.value.length!=0 ) {

              if($(this).attr("class").includes("revenues")){
                      rev=parseFloat(normal_format($(this).val()));
               }

              else
              {
                       rev=parseFloat(normal_format($(this).siblings(".revenues").val()));
              }

              if(!rev )
              { rev=0;}

              revperc=parseFloat((rev/rev)*100).toFixed(2);
              if(isNaN(revperc)){
                    revperc = 0;
               }

	          if($(this).attr("class").includes("materialexpenses"))
               {
                      matexp=parseFloat(normal_format($(this).val()));
               }
               else
               {
                     matexp=parseFloat(normal_format($(this).siblings(".materialexpenses").val()));
               }
	         if(!matexp )
              { matexp=0;}
             matexpperc=parseFloat((matexp/rev)*100).toFixed(2);
             if(isNaN(matexpperc)){
                     matexpperc = 0;
                }

             if($(this).attr("class").includes("personnelexpenses"))
               {
                persexp=parseFloat(normal_format($(this).val()));
               }
                else
               {
               persexp=parseFloat(normal_format($(this).siblings(".personnelexpenses").val()));
              }


	         if(!persexp )
            { persexp=0;}
            persexpperc=parseFloat((persexp/rev)*100).toFixed(2);
            if(isNaN(persexpperc)){
              persexpperc = 0;
             }

           if($(this).attr("class").includes("otherexpenses"))
            {
              otherexp=parseFloat(normal_format($(this).val()));
            }
            else
            {
             otherexp=parseFloat(normal_format($(this).siblings(".otherexpenses").val()));
            }

	       if(isNaN(otherexp))
            { otherexp=0;}
            otherexpperc=parseFloat((otherexp/rev)*100).toFixed(2);
            if(isNaN(otherexpperc)){
               otherexpperc = 0;
             }

           if($(this).attr("class").includes("otherincome"))
            {
               otherincomes=parseFloat(normal_format($(this).val()));
            }
           else
            {
               otherincomes=parseFloat(normal_format($(this).siblings(".otherincome").val()));
             }
	       if(!otherincomes )
            { otherincomes=0;}
           otherincomesperc=parseFloat((otherincomes/rev)*100).toFixed(2);
           if(isNaN(otherincomesperc)){
             otherincomesperc = 0;
            }

	       if($(this).attr("class").includes("depreciation"))
           {
             depreciationval=parseFloat(normal_format($(this).val()));
            }
           else
            {
               depreciationval=parseFloat(normal_format($(this).siblings(".depreciation").val()));
            }
	       if(!depreciationval)
           { depreciationval=0;}
            depreciationperc=parseFloat((depreciationval/rev)*100).toFixed(2);
           if(isNaN(depreciationperc)){
           depreciationperc = 0;
           }

   	       if($(this).attr("class").includes("interestexpenses"))
           {
             interestexp=parseFloat(normal_format($(this).val()));
           }
           else
           {
             interestexp=parseFloat(normal_format($(this).siblings(".interestexpenses").val()));
           }
	        if(!interestexp)
            { interestexp=0;}

           interestexpperc=parseFloat((interestexp/rev)*100).toFixed(2);
           if(isNaN(interestexpperc)){
             interestexpperc = 0;
            }

	       if($(this).attr("class").includes("interestincome"))
           {
            interestincomes=parseFloat(normal_format($(this).val()));
           }
           else
            {
              interestincomes=parseFloat(normal_format($(this).siblings(".interestincome").val()));
            }
	        if(!interestincomes)
             { interestincomes=0;}

            interestincomesperc=parseFloat((interestincomes/rev)*100).toFixed(2);

            if(isNaN(interestincomesperc)){
             interestincomesperc = 0;
            }


	        if($(this).attr("class").includes("incometax"))
            {
             incometaxs=parseFloat(normal_format($(this).val()));
            }
            else
             {
               incometaxs=parseFloat(normal_format($(this).siblings(".incometax").val()));
             }
	        if(!incometaxs)
             { incometaxs=0;}

           incometaxsperc=parseFloat((incometaxs/rev)*100).toFixed(2);
            if(isNaN(incometaxsperc)){
             incometaxsperc = 0;
            }

            var valebitda=(rev+otherincomes)-(matexp+ persexp + otherexp);
            var   ebitdaperc=parseFloat((valebitda/rev)*100).toFixed(2);
            if(isNaN(ebitdaperc)){
                   ebitdaperc = 0;
             }
            var valebitdamargin=parseFloat((valebitda/rev)*100).toFixed(2);
            if(isNaN(valebitdamargin)){
            valebitdamargin = 0;
           }


           var valebit=parseFloat(valebitda-depreciationval);

           if(isNaN(ebitperc)){
           ebitperc = 0;
           }

           var valebitmargin=parseFloat((valebit/rev)*100).toFixed(2);

           if(isNaN(valebitmargin)){
           valebitmargin = 0;
           }

             var valebt=parseFloat((valebit-interestexp)+interestincomes);
             var valnetincome=parseFloat(valebt-incometaxs);
             var pernetincome=parseFloat((valnetincome/rev)*100).toFixed(2);
             $(".txt").css("width","60%");
             $(".txt1").css("font-size","75%");


            $(this).siblings(".ebitda").val(german_format(valebitda));
            $(this).siblings(".ebitdamargin").val(valebitdamargin);
            $(this).siblings(".ebit").val(german_format(valebit));
            $(this).siblings(".ebitmargin").val(valebitmargin);
            $(this).siblings(".ebt").val(german_format(valebt));
            $(this).siblings(".netincome").val(german_format(valnetincome));


              $(this).siblings(".revenuesper").html("&nbsp;<b>"+revperc+"%");
              $(this).siblings(".materialexpensesper").html("&nbsp;<b>"+matexpperc+"%</b>");
              $(this).siblings(".personnelexpensesper").html("&nbsp;<b>"+persexpperc+"%</b>");
              $(this).siblings(".otherexpensesper").html("&nbsp;<b>"+otherexpperc+"%</b>");
              $(this).siblings(".otherincomeper").html("&nbsp;<b>"+otherincomesperc+"%</b>");
              $(this).siblings(".depreciationper").html("&nbsp;<b>"+depreciationperc+"%</b>");
              $(this).siblings(".interestexpensesper").html("&nbsp;<b>"+interestexpperc+"%</b>");
              $(this).siblings(".interestincomeper").html("&nbsp;<b>"+interestincomesperc+"%</b>");
              $(this).siblings(".incometaxper").html("&nbsp;<b>"+incometaxsperc+"%</b>");
              $(this).siblings(".netincomeper").html("&nbsp;<b>"+pernetincome+"%</b>");


            if($(this).attr("class").includes("fixedasset"))
            {
             valfixedasset=parseFloat(normal_format($(this).val()));
            }
            else
             {
             valfixedasset=parseFloat(normal_format($(this).siblings(".fixedasset").val()));
            }
            if(!valfixedasset )
             { valfixedasset=0;}


            if($(this).attr("class").includes("finanacialasset"))
            {
              valfinanacialasset=parseFloat(normal_format($(this).val()));
            }
            else
            {
              valfinanacialasset=parseFloat(normal_format($(this).siblings(".finanacialasset").val()));
            }
           if(!valfinanacialasset )
            { valfinanacialasset=0;}
            if($(this).attr("class").includes("inventories"))
           {
           valinventories=parseFloat(normal_format($(this).val()));
           }
         else
           {
           valinventories=parseFloat(normal_format($(this).siblings(".inventories").val()));
           }
           if(!valinventories )
          { valinventories=0;}
          if($(this).attr("class").includes("trade"))
          {
          valtrade=parseFloat(normal_format($(this).val()));
          }
          else
          {
          valtrade=parseFloat(normal_format($(this).siblings(".trade").val()));
          }
           if(!valtrade )
           { valtrade=0;}

          if($(this).attr("class").includes("cash"))
          {
          valcash=parseFloat(normal_format($(this).val()));
          }
          else
          {
          valcash=parseFloat(normal_format($(this).siblings(".cash").val()));
          }
            if(!valcash )
          { valcash=0;}
          if($(this).attr("class").includes("othercurrentassets"))
          {
         valothercurrentassets=parseFloat(normal_format($(this).val()));
         }
         else
         {
         valothercurrentassets=parseFloat(normal_format($(this).siblings(".othercurrentassets").val()));
         }
           if(!valothercurrentassets )
           { valothercurrentassets=0;}
            if($(this).attr("class").includes("equity"))
                {
                   valequity=parseFloat(normal_format($(this).val()));
              }
               else
             {
               valequity=parseFloat(normal_format($(this).siblings(".equity").val()));
             }

           if(!valequity )
           { valequity=0;}
            if($(this).attr("class").includes("financialdebt"))
                {
                   valfinancialdebt=parseFloat(normal_format($(this).val()));
              }
               else
             {
               valfinancialdebt=parseFloat(normal_format($(this).siblings(".financialdebt").val()));
             }
           if(!valfinancialdebt )
           { valfinancialdebt=0;}


            if($(this).attr("class").includes("pensionprovisions"))
                {
                   valpensionprovisions=parseFloat(normal_format($(this).val()));
              }
               else
             {
               valpensionprovisions=parseFloat(normal_format($(this).siblings(".pensionprovisions").val()));
             }
           if(!valpensionprovisions )
          { valpensionprovisions=0;}

            if($(this).attr("class").includes("otherprovisions"))
                {
                   valotherprovisions=parseFloat(normal_format($(this).val()));
              }
               else
             {
               valotherprovisions=parseFloat(normal_format($(this).siblings(".otherprovisions").val()));
             }
           if(!valotherprovisions )
          { valotherprovisions=0;}

            if($(this).attr("class").includes("tradepayables"))
                {
                   valtradepayables=parseFloat(normal_format($(this).val()));
              }
               else
             {
               valtradepayables=parseFloat(normal_format($(this).siblings(".tradepayables").val()));
             }
            if(!valtradepayables )
              { valtradepayables=0;}

            if($(this).attr("class").includes("otherliabilities"))
                {
                   valotherliabilities=parseFloat(normal_format($(this).val()));
                }
               else
               {
               valotherliabilities=parseFloat(normal_format($(this).siblings(".otherliabilities").val()));
              }
            if(!valotherliabilities )
               { valotherliabilities=0;}
            $(this).siblings(".totalcurrentassets").val(german_format(valinventories+valothercurrentassets+valcash+valtrade));
             $(this).siblings(".totalassets").val(german_format(valinventories+valothercurrentassets+valcash+valtrade+valfixedasset+valfinanacialasset));




          $(this).siblings(".totalliabilities").val(german_format(valfinancialdebt+valpensionprovisions+valotherprovisions+valtradepayables+valotherliabilities));

          $(this).siblings(".totalequity").val(german_format(valequity+valfinancialdebt+valpensionprovisions+valotherprovisions+valtradepayables+valotherliabilities));
          debugger;
          if($(this).attr("class").includes("capitalexpendituresinfixedasset"))
         {
         valcapitalexpendituresinfixedasset=parseFloat(normal_format($(this).val()));
         }
         else
         {
         valcapitalexpendituresinfixedasset=parseFloat(normal_format($(this).siblings(".capitalexpendituresinfixedasset").val()));
         }
           valcapitalexpendituresinfixedasset=parseFloat(normal_format(valcapitalexpendituresinfixedasset));
           if(!valcapitalexpendituresinfixedasset )
           { valcapitalexpendituresinfixedasset=0;}

          if(valinventories == 0){
           if($(this).parent().attr("class").indexOf("income_statement")!=-1)
             yr =$(this).parent().attr("class").slice(30,);

             if($(this).parent().attr("class").indexOf("balance_sheet")!=-1)
             yr =$(this).parent().attr("class").slice(27,);

          if($(this).parent().attr("class").indexOf("investment")!=-1)
             yr =$(this).parent().attr("class").slice(26,);
          $(".revenues").each(function(){
         if($(this).attr("class").includes(yr))
         {
         rev=parseFloat(normal_format($(this).val()));
         }


          })
          }
            var valcapitalexpenditurepercent= ((valcapitalexpendituresinfixedasset/rev)*100).toFixed(2);
           $(this).siblings(".capitalexpenditurepercent").val(valcapitalexpenditurepercent);
           if(valinventories == 0){
           if($(this).parent().attr("class").indexOf("income_statement")!=-1)
             yr =$(this).parent().attr("class").slice(30,);

             if($(this).parent().attr("class").indexOf("balance_sheet")!=-1)
             yr =$(this).parent().attr("class").slice(27,);

          if($(this).parent().attr("class").indexOf("investment")!=-1)
             yr =$(this).parent().attr("class").slice(26,);
          $(".inventories").each(function(){
         if($(this).attr("class").includes(yr))
         {
         valinventories=parseFloat(normal_format($(this).val()));
          valtrade=parseFloat(normal_format($(this).siblings(".trade").val()));
         valothercurrentassets=parseFloat(normal_format($(this).siblings(".othercurrentassets").val()));
         valcash=parseFloat(normal_format($(this).siblings(".cash").val()));

         }


          })
          }


           valnetcurrentassets=(valinventories+valtrade+valothercurrentassets+valcash);
           $(this).siblings(".netcurrentassets").val(german_format(valnetcurrentassets));
           debugger;
             $(this).siblings(".netcurrentassetspercent").val(german_format((parseFloat(valnetcurrentassets)/parseFloat(rev))*100)+"%");








	 }
    });
 })