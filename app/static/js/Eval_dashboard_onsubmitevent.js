<script type="text/javascript">
$(document).ready(function(){

     $("#submit").click(function(){
             var valyear=0,useronline=0,valrevenue=0,valmaterialexpenses=0,valpersonnelexpenses=0,valotherexpenses=0,valotherincome=0,valebitda=0,valebitdamargin=0,valdepreciation=0,valebit=0,valebitmargin=0,valinterestexpenses=0,valinterestincome=0,valebt=0,valincometax=0,valnetincome=0;
             var valfixedasset=0,valfinanacialasset=0,valinventories=0,valtrade=0,valothercurrentassets=0,valcash=0,valtotalcurrentassets=0,valtotalassets=0,valequity=0,valfinancialdebt=0,valpensionprovisions=0,valotherprovisions=0,valtradepayables=0,valotherliabilities=0,valtotalliabilities=0,valtotalequity=0,valcapitalexpendituresinfixedasset=0,valcapitalexpenditurepercent=0,valnetcurrentassets =0,valnetcurrentassetspercent=0;
      $("div").each(function(){

        if($(this).attr("class").indexOf("year")!=-1){

                    valyear=$(this).attr("class");
                    useronline="{{currentuser}}";
                    valrevenue=parseFloat(normal_format($(this).children(".revenues").val()));
                    valmaterialexpenses=parseFloat(normal_format($(this).children(".materialexpenses").val()));
                    valpersonnelexpenses=parseFloat(normal_format($(this).children(".personnelexpenses").val()));
                    valotherexpenses=parseFloat(normal_format($(this).children(".otherexpenses").val()));
                    valotherincome=parseFloat(normal_format($(this).children(".otherincome").val()));
                    valebitda=parseFloat(normal_format($(this).children(".ebitda").val()));
                    valebitdamargin=parseFloat(($(this).children(".ebitdamargin").val()));
                    valdepreciation=parseFloat(normal_format($(this).children(".depreciation").val()));
                    valebit=parseFloat(normal_format($(this).children(".ebit").val()));
                    valebitmargin=parseFloat($(this).children(".ebitmargin").val());
                    valinterestexpenses=parseFloat(normal_format($(this).children(".interestexpenses").val()));
                    valinterestincome=parseFloat(normal_format($(this).children(".interestincome").val()));
                    valebt=parseFloat(normal_format($(this).children(".ebt").val()));
                    valincometax=parseFloat(normal_format($(this).children(".incometax").val()));
                    valnetincome=parseFloat(normal_format($(this).children(".netincome").val()));





                      valfixedasset=parseFloat(normal_format($(this).children(".fixedasset").val()));

                    valfinanacialasset=parseFloat(normal_format($(this).children(".finanacialasset").val()));
                     valinventories=parseFloat(normal_format($(this).children(".inventories").val()));
                     valtrade=parseFloat(normal_format($(this).children(".trade").val()));
                     valothercurrentassets=parseFloat(normal_format($(this).children(".othercurrentassets").val()));
                     valcash=parseFloat(normal_format($(this).children(".cash").val()));
                     valtotalcurrentassets=parseFloat(($(this).children(".totalcurrentassets").val()));
                     valtotalassets=parseFloat(normal_format($(this).children(".totalassets").val()));
                     valequity=parseFloat(normal_format($(this).children(".equity").val()));
                     valfinancialdebt=parseFloat($(this).children(".financialdebt").val());
                     valpensionprovisions=parseFloat(normal_format($(this).children(".pensionprovisions").val()));
                     valotherprovisions=parseFloat(normal_format($(this).children(".otherprovisions").val()));
                     valtradepayables=parseFloat(normal_format($(this).children(".tradepayables").val()));
                     valotherliabilities=parseFloat(normal_format($(this).children(".otherliabilities").val()));
                     valtotalliabilities=parseFloat(normal_format($(this).children(".totalliabilities").val()));
                    valtotalequity=parseFloat(normal_format($(this).children(".totalequity").val()));

                     valcapitalexpendituresinfixedasset=parseFloat(normal_format($(this).children(".capitalexpendituresinfixedasset").val()));
                     valcapitalexpenditurepercent=parseFloat(normal_format($(this).children(".capitalexpenditurepercent").val()));
                     valnetcurrentassets=parseFloat(normal_format($(this).children(".netcurrentassets").val()));
                    valnetcurrentassetspercent=parseFloat(normal_format($(this).children(".netcurrentassetspercent").val()));
                    useronline="{{currentuser}}";

       $.ajax({
          type: "POST",
          dataType: 'json',
          async: true,
          cache: false,

          url: "/EvalCompanyDashboard/datasubmit/"+useronline,
          data:
          {
          "nowuser":useronline,
          "years":valyear,
          "revenue":valrevenue,
          "materialexpenses":valmaterialexpenses,
          "personnelexpenses":valpersonnelexpenses,
          "otherexpenses":valotherexpenses,
          "otherincome":valotherincome,
          "ebitda":valebitda,
          "ebitdamargin":valebitdamargin,
          "depreciation":valdepreciation,
          "ebit":valebit,
          "ebitmargin":valebitmargin,
          "interestexpenses":valinterestexpenses,
          "interestincome":valinterestincome,
          "ebt":valebt,
          "incometax":valincometax,
          "netincome":valnetincome,


          "fixedasset":valfixedasset,
          "finanacialasset":valfinanacialasset,
          "inventories":valinventories,
          "trade":valtrade,
          "othercurrentassets":valothercurrentassets,
          "cash":valcash,
          "totalcurrentassets":valtotalcurrentassets,
          "totalassets":valtotalassets,
          "equity":valequity,
          "financialdebt":valfinancialdebt,
          "pensionprovisions":valpensionprovisions,
          "otherprovisions":valotherprovisions,
          "tradepayables":valtradepayables,
          "otherliabilities":valotherliabilities,
          "totalliabilities":valtotalliabilities,
          "totalequity":valtotalequity,

          "capitalexpendituresinfixedasset":valcapitalexpendituresinfixedasset,
          "capitalexpenditurepercent":valcapitalexpenditurepercent,
          "netcurrentassets":valnetcurrentassets,
          "netcurrentassetspercent":valnetcurrentassetspercent,
           }
       });
       }

     });
   });

 })
 </script>
