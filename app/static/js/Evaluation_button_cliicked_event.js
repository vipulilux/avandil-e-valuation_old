$(document).ready(function(){
buttonstate="{{buttononoff}}";
latestdata="";
$('.ulclick').click(function(){

   //button state off


     if (buttonstate == 'off') {
      //$("#evaluationform").submit();
       var yrplus=0;
       var yr=0;
       $("div").each(function(){

          if($(this).attr("class").indexOf("income_statement year")!=-1)
            {
  //////////////////////////////////////////////////////
             if(yr > new Date().getFullYear())
               {
               $(this).children(".revenues").val(german_format(0));
             $(this).children(".revenuesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".materialexpenses").val(german_format(0));
             $(this).children(".materialexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".personnelexpenses").val(german_format(0));
             $(this).children(".personnelexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherexpenses").val(german_format(0));
             $(this).children(".otherexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherincome").val(german_format(0));
             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");

             $(this).children(".depreciation").val(german_format(0));
             $(this).children(".depreciationper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".incometax").val(german_format(0));
             $(this).children(".incometaxper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincome").val(german_format(0));
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitdamargin").val(german_format(0));
             $(this).children(".ebitdamarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
             $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestexpenses").val(german_format(0));
             $(this).children(".interestexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestincome").val(german_format(0));

             $(this).children(".interestincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitmargin").val(german_format(0));
             $(this).children(".ebitmarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");
          //   $(this).children(".ebitdamargin").val("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
           //  $(this).children(".ebitmargin").val("&nbsp;"+"0.00"+"%");
             $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             //$(this).children(".netincome").val("&nbsp;"+"0.00"+"%");
               }
             yr =$(this).attr("class").slice(30,);
             var currentTime = new Date();
             yrplus=yrplus+1;

              //for current year data retrival
              rev=parseFloat("{{companydata[0].pertotalrevenue}}");
              if(rev==0)
              {


                $("#percentcalcuation2").show();
                $("#income_statement_data").hide();
                $("#submitbtn").hide();

              }


              if (yr == new Date().getFullYear())
             {
             if(latestdata!="")
             {
             debugger;
              var rev=parseFloat(latestdata.totalrevenue);
             $(this).children(".revenues").val(german_format(latestdata.totalrevenue));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");
               $(this).children(".materialexpenses").val(german_format(latestdata.materialexp));
              var materialexpenses=parseFloat(latestdata.materialexp);
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format(latestdata.personalexp));
             var personnelexpenses=parseFloat(latestdata.personalexp);
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format(latestdata.otherexp));

             var otherexpenses=parseFloat(latestdata.otherexp);
             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format(latestdata.otherincome));

             var otherincome=parseFloat(latestdata.otherincome);
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
              ebitda=parseFloat((rev+otherincome)-(otherexpenses+personnelexpenses+materialexpenses));
             $(this).children(".ebitda").val(german_format(ebitda));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitda/rev)*100).toFixed(2));


             $(this).children(".depreciation").val(german_format(latestdata.depreciation));
              depreciation=parseFloat(latestdata.depreciation);
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             ebit=parseFloat(ebitda)- parseFloat(depreciation);


             $(this).children(".ebit").val(german_format(ebit));
             $(this).children(".ebtper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");

             $(this).children(".ebitmargin").val(((ebit/rev)*100).toFixed(2));


            $(this).children(".interestexpenses").val(german_format(latestdata.interestexp));
                          var interestexpenses=parseFloat(latestdata.interestexp);
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");

                          $(this).children(".interestincome").val(german_format(latestdata.interestincome));
             var interestincome=parseFloat(latestdata.interestincome);
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             ebt=(ebit+interestincome)-interestexpenses;
             $(this).children(".ebtper").val(german_format(ebt));

              $(this).children(".ebtper").val(((ebt/totalrev)*100).toFixed(2)+"%");


             $(this).children(".incometax").val(german_format(latestdata.incometaxes));

              var incometax=parseFloat(latestdata.incometaxes);
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(latestdata.netincome));

              var netincome=parseFloat(latestdata.netincome);
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
          //
                   netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             else
             {
          var totalrev=parseFloat("{{companydata[0].totalrevenue}}");

             var rev=parseFloat("{{companydata[0].totalrevenue}}");
             $(this).children(".revenues").val(german_format("{{companydata[0].totalrevenue}}"));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");

             $(this).children(".materialexpenses").val(german_format("{{companydata[0].materialexp}}"));
              var materialexpenses=parseFloat("{{companydata[0].materialexp}}");
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format("{{companydata[0].personalexp}}"));
             var personnelexpenses=parseFloat("{{companydata[0].personalexp}}");
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format("{{companydata[0].otherexp}}"));

             var otherexpenses=parseFloat("{{companydata[0].otherexp}}");
             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format("{{companydata[0].otherincome}}"));

             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitda").val(german_format("{{companydata[0].ebitda}}"));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");

             $(this).children(".depreciation").val(german_format("{{companydata[0].depreciation}}"));
              var depreciation=parseFloat("{{companydata[0].depreciation}}");
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             $(this).children(".ebt").val(german_format("{{companydata[0].ebt}}"));

              var ebt=parseFloat("{{companydata[0].ebt}}");
             $(this).children(".ebtper").html("&nbsp;"+((ebt/rev)*100).toFixed(2)+"%");
             $(this).children(".incometax").val(german_format("{{companydata[0].incometaxes}}"));

              var incometax=parseFloat("{{companydata[0].incometaxes}}");
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format("{{companydata[0].netincome}}"));

              var netincome=parseFloat("{{companydata[0].netincome}}");
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(german_format("{{companydata[0].ebitdamargin}}"));

              var ebitdamargin=parseFloat("{{companydata[0].ebitdamargin}}");
             $(this).children(".ebitdamarginper").html("&nbsp;"+((ebitdamargin/rev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format("{{companydata[0].ebit}}"));

              var ebit=parseFloat("{{companydata[0].ebit}}");
             $(this).children(".ebitper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");
             $(this).children(".interestexpenses").val(german_format("{{companydata[0].interestexp}}"));

              var interestexpenses=parseFloat("{{companydata[0].interestexp}}");
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".interestincome").val(german_format("{{companydata[0].interestincome}}"));
             var interestincome=parseFloat("{{companydata[0].interestincome}}");
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitmargin").val(german_format("{{companydata[0].ebitmargin}}"));
             var ebitmargin=parseFloat("{{companydata[0].ebitmargin}}");
             $(this).children(".ebitmarginper").html("&nbsp;"+((ebitmargin/rev)*100).toFixed(2)+"%");

              ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
             $(this).children(".ebitda").val(german_format(ebitdaval));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");

             ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
             $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format(ebitval));
             $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
             ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

             $(this).children(".ebt").val(german_format(ebtval));
             $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

              netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             }

             //for other year data
             else
             {

             var rev=0;
             var mexp=0;
             var pexp=0;
             var oexp=0;
             var oincome=0;
             var dep=0;
             var intexp=0;
             var intintincome=0;
             var intax=0;
             if ($(this).prev().children(".revenues").length==0)
             {
              rev=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".revenues").val()));

             }
             else
             {

              rev=parseFloat(normal_format($(this).prev().children(".revenues").val()));
             }
            totalrev=(rev*(parseFloat("{{companydata[0].pertotalrevenue}}")+100))/100;
              $(this).children(".revenues").val(german_format(totalrev));
             $(this).children(".revenuesper").html("&nbsp;"+((totalrev/totalrev)*100).toFixed(2)+"%");

              if ($(this).prev().children(".materialexpenses").length==0)
             {
              mexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".materialexpenses").val()));

             }
             else
             {

              mexp=parseFloat(normal_format($(this).prev().children(".materialexpenses").val()));
             }

            totalmexp=(mexp*(parseFloat("{{companydata[0].permaterialexp}}")+100))/100;
              $(this).children(".materialexpenses").val(german_format(totalmexp));
             $(this).children(".materialexpensesper").html("&nbsp;"+((totalmexp/totalrev)*100).toFixed(2)+"%");

             if ($(this).prev().children(".personnelexpenses").length==0)
             {
              pexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".personnelexpenses").val()));

             }
             else
             {

              pexp=parseFloat(normal_format($(this).prev().children(".personnelexpenses").val()));
             }

            totalpexp=(pexp*(parseFloat("{{companydata[0].perpersonalexp}}")+100))/100;
              $(this).children(".personnelexpenses").val(german_format(totalpexp));
             $(this).children(".personnelexpensesper").html("&nbsp;"+((totalpexp/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".otherexpenses").length==0)
             {
              oexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".otherexpenses").val()));

             }
             else
             {

              oexp=parseFloat(normal_format($(this).prev().children(".otherexpenses").val()));
             }

            totaloexp=(oexp*(parseFloat("{{companydata[0].perotherexp}}")+100))/100;
              $(this).children(".otherexpenses").val(german_format(totaloexp));
             $(this).children(".otherexpensesper").html("&nbsp;"+((totaloexp/totalrev)*100).toFixed(2)+"%");

             if ($(this).prev().children(".otherincome").length==0)
             {
              oincome=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".otherincome").val()));

             }
             else
             {

              oincome=parseFloat(normal_format($(this).prev().children(".otherincome").val()));
             }

            totaloincome=(oincome*(parseFloat("{{companydata[0].perotherincome}}")+100))/100;
              $(this).children(".otherincome").val(german_format(totaloincome));
             $(this).children(".otherincomeper").html("&nbsp;"+((totaloincome/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".depreciation").length==0)
             {
              dep=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".depreciation").val()));

             }
             else
             {

              dep=parseFloat(normal_format($(this).prev().children(".depreciation").val()));
             }

            totaldep=(dep*(parseFloat("{{companydata[0].perdepreciation}}")+100))/100;
              $(this).children(".depreciation").val(german_format(totaldep));
             $(this).children(".depreciationper").html("&nbsp;"+((totaldep/totalrev)*100).toFixed(2)+"%");


             if ($(this).prev().children(".interestexpenses").length==0)
             {
              intexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".interestexpenses").val()));

             }
             else
             {

              intexp=parseFloat(normal_format($(this).prev().children(".interestexpenses").val()));
             }

            totalintexp=(intexp*(parseFloat("{{companydata[0].perinterestexp}}")+100))/100;
              $(this).children(".interestexpenses").val(german_format(totalintexp));
             $(this).children(".interestexpensesper").html("&nbsp;"+((totalintexp/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".interestincome").length==0)
             {
              intintincome=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".interestincome").val()));

             }
             else
             {

              intintincome=parseFloat(normal_format($(this).prev().children(".interestincome").val()));
             }

            totalintincome=(intintincome*(parseFloat("{{companydata[0].perinterestincome}}")+100))/100;
              $(this).children(".interestincome").val(german_format(totalintincome));
             $(this).children(".interestincomeper").html("&nbsp;"+((totalintincome/totalrev)*100).toFixed(2)+"%");


             if ($(this).prev().children(".incometax").length==0)
             {
              intax=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".incometax").val()));

             }
             else
             {

              intax=parseFloat(normal_format($(this).prev().children(".incometax").val()));
             }

            totalintax=(intax*(parseFloat("{{companydata[0].incometaxes}}")+100))/100;
              $(this).children(".incometax").val(german_format(totalintax));
             $(this).children(".incometaxper").html("&nbsp;"+((totalintax/totalrev)*100).toFixed(2)+"%");

             ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
             $(this).children(".ebitda").val(german_format(ebitdaval));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");

             ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
             $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format(ebitval));
             $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
             ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

             $(this).children(".ebt").val(german_format(ebtval));
             $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

              netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             }
             });
             $('#activeSwitch').prop('checked', false).change();
             buttonstate="on";
             useronline="{{currentuser}}";
               $("div").each(function(){
                        if($(this).attr("class").indexOf("income_statement year"+(new Date().getFullYear()))!=-1)
                         {
                         useronline="{{currentuser}}";

                         tr=parseFloat(normal_format($(this).children(".revenues").val()));
                         me=parseFloat(normal_format($(this).children(".materialexpenses").val()));
                         pe=parseFloat(normal_format($(this).children(".personnelexpenses").val()));
                         oe=parseFloat(normal_format($(this).children(".otherexpenses").val()));
                         oi=parseFloat(normal_format($(this).children(".otherexpenses").val()));
                         dep=parseFloat(normal_format($(this).children(".depreciation").val()));
                         ie=parseFloat(normal_format($(this).children(".interestexpenses").val()));
                         ii=parseFloat(normal_format($(this).children(".interestincome").val()));
                         it=parseFloat(normal_format($(this).children(".incometax").val()));

                         $.ajax({
                              type: "POST",
                              dataType: 'json',
                              async: true,
                              cache: false,
                             url: "/EvalCompanyDashboards/buttonclk/"+useronline,
                             data:
                               {

                                     "buttonstates":buttonstate,
                                     "totalrevenue":tr,
                                     "materialexp":me,
                                     "personalexp":pe,
                                     "otherexp":oe,
                                     "otherincome":oi,
                                     "depreciation":dep,
                                     "interestexp":ie,
                                     "interestincome":ii,
                                     "incometaxes":it,
                               }
                         })
                         }
                       })
}


//button state on
  else if (buttonstate == 'on') {

    var yrplus=0;


     $("div").each(function(){
     if($(this).attr("class").indexOf("income_statement year"+(new Date().getFullYear()+yrplus))!=-1)
       {
              var yr=0;
              yr =$(this).attr("class").slice(30,);
              var currentTime = new Date();
              yrplus=yrplus+1;

              //current year data retrival
              if (yr == new Date().getFullYear())


              { debugger;
                  if(latestdata!="")
             {

              var rev=parseFloat(latestdata.totalrevenue);
             $(this).children(".revenues").val(german_format(latestdata.totalrevenue));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");
               $(this).children(".materialexpenses").val(german_format(latestdata.materialexp));
              var materialexpenses=parseFloat(latestdata.materialexp);
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format(latestdata.personalexp));
             var personnelexpenses=parseFloat(latestdata.personalexp);
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format(latestdata.otherexp));

             var otherexpenses=parseFloat(latestdata.otherexp);
             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format(latestdata.otherincome));

             var otherincome=parseFloat(latestdata.otherincome);
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
              ebitda=parseFloat((rev+otherincome)-(otherexpenses+personnelexpenses+materialexpenses));
             $(this).children(".ebitda").val(german_format(ebitda));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitda/rev)*100).toFixed(2));


             $(this).children(".depreciation").val(german_format(latestdata.depreciation));
              depreciation=parseFloat(latestdata.depreciation);
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             ebit=parseFloat(ebitda)- parseFloat(depreciation);


             $(this).children(".ebit").val(german_format(ebit));
             $(this).children(".ebtper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");

             $(this).children(".ebitmargin").val(((ebit/rev)*100).toFixed(2));


            $(this).children(".interestexpenses").val(german_format(latestdata.interestexp));
                          var interestexpenses=parseFloat(latestdata.interestexp);
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");

                          $(this).children(".interestincome").val(german_format(latestdata.interestincome));
             var interestincome=parseFloat(latestdata.interestincome);
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             ebt=(ebit+interestincome)-interestexpenses;
             $(this).children(".ebtper").val(german_format(ebt));

              $(this).children(".ebtper").val(((ebt/totalrev)*100).toFixed(2)+"%");


             $(this).children(".incometax").val(german_format(latestdata.incometaxes));

              var incometax=parseFloat(latestdata.incometaxes);
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(latestdata.netincome));

              var netincome=parseFloat(latestdata.netincome);
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
          //
                   netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             else{
                 var totalrev=parseFloat("{{companydata[0].totalrevenue}}");
                 var rev=parseFloat("{{companydata[0].totalrevenue}}");
                 $(this).children(".revenues").val(german_format("{{companydata[0].totalrevenue}}"));
                 $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");

                 $(this).children(".materialexpenses").val(german_format("{{companydata[0].materialexp}}"));
                 var materialexpenses=parseFloat("{{companydata[0].materialexp}}");
                 $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");

                $(this).children(".personnelexpenses").val(german_format("{{companydata[0].personalexp}}"));
                var personnelexpenses=parseFloat("{{companydata[0].personalexp}}");
                $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");

                $(this).children(".otherexpenses").val(german_format("{{companydata[0].otherexp}}"));
                var otherexpenses=parseFloat("{{companydata[0].otherexp}}");
                $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");

                 $(this).children(".otherincome").val(german_format("{{companydata[0].otherincome}}"));
                 var otherincome=parseFloat("{{companydata[0].otherincome}}");
                 $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");

                 $(this).children(".ebitda").val(german_format("{{companydata[0].ebitda}}"));
                 var ebitda=parseFloat("{{companydata[0].ebitda}}");
                 $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");

                 $(this).children(".depreciation").val(german_format("{{companydata[0].depreciation}}"));
                 var depreciation=parseFloat("{{companydata[0].depreciation}}");
                 $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");

                 $(this).children(".ebt").val(german_format("{{companydata[0].ebt}}"));
                 var ebt=parseFloat("{{companydata[0].ebt}}");
                 $(this).children(".ebtper").html("&nbsp;"+((ebt/rev)*100).toFixed(2)+"%");

                 $(this).children(".incometax").val(german_format("{{companydata[0].incometaxes}}"));
                 var incometax=parseFloat("{{companydata[0].incometaxes}}");
                 $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
                 $(this).children(".netincome").val(german_format("{{companydata[0].netincome}}"));

                 var netincome=parseFloat("{{companydata[0].netincome}}");
                 $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
                 $(this).children(".ebitdamargin").val(german_format("{{companydata[0].ebitdamargin}}"));

                 var ebitdamargin=parseFloat("{{companydata[0].ebitdamargin}}");
                 $(this).children(".ebitdamarginper").html("&nbsp;"+((ebitdamargin/rev)*100).toFixed(2)+"%");
                 $(this).children(".ebit").val(german_format("{{companydata[0].ebit}}"));

                 var ebit=parseFloat("{{companydata[0].ebit}}");
                 $(this).children(".ebitper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");

                 $(this).children(".interestexpenses").val(german_format("{{companydata[0].interestexp}}"));
                 var interestexpenses=parseFloat("{{companydata[0].interestexp}}");
                 $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");

                 $(this).children(".interestincome").val(german_format("{{companydata[0].interestincome}}"));
                 var interestincome=parseFloat("{{companydata[0].interestincome}}");
                 $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");

                 $(this).children(".ebitmargin").val(german_format("{{companydata[0].ebitmargin}}"));
                 var ebitmargin=parseFloat("{{companydata[0].ebitmargin}}");
                 $(this).children(".ebitmarginper").html("&nbsp;"+((ebitmargin/rev)*100).toFixed(2)+"%");

                ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
                $(this).children(".ebitda").val(german_format(ebitdaval));
                $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");

                 $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");
                 ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
                 $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
                 $(this).children(".ebit").val(german_format(ebitval));
                 $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
                 ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

                $(this).children(".ebt").val(german_format(ebtval));
                $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

                netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
                $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
                $(this).children(".netincome").val(german_format(netloss));
                }
             }
             else
             {

             $(this).children(".revenues").val(german_format(0));
             $(this).children(".revenuesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".materialexpenses").val(german_format(0));
             $(this).children(".materialexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".personnelexpenses").val(german_format(0));
             $(this).children(".personnelexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherexpenses").val(german_format(0));
             $(this).children(".otherexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherincome").val(german_format(0));
             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");

             $(this).children(".depreciation").val(german_format(0));
             $(this).children(".depreciationper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".incometax").val(german_format(0));
             $(this).children(".incometaxper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincome").val(german_format(0));
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitdamargin").val(german_format(0));
             $(this).children(".ebitdamarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
             $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestexpenses").val(german_format(0));
             $(this).children(".interestexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestincome").val(german_format(0));

             $(this).children(".interestincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitmargin").val(german_format(0));
             $(this).children(".ebitmarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");
          //   $(this).children(".ebitdamargin").val("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
           //  $(this).children(".ebitmargin").val("&nbsp;"+"0.00"+"%");
             $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             //$(this).children(".netincome").val("&nbsp;"+"0.00"+"%");

             }
             }

             });
                       $('#activeSwitch').prop('checked', true).change();

                       buttonstate="off";
                       $("div").each(function(){
                        if($(this).attr("class").indexOf("income_statement year"+(new Date().getFullYear()))!=-1)
                         {
                         useronline="{{currentuser}}";
                         tr=parseFloat(normal_format($(this).children(".revenues").val()));
                         me=parseFloat(normal_format($(this).children(".materialexpenses").val()));
                         pe=parseFloat(normal_format($(this).children(".personnelexpenses").val()));
                         oe=parseFloat(normal_format($(this).children(".otherexpenses").val()));
                         oi=parseFloat(normal_format($(this).children(".otherexpenses").val()));
                         dep=parseFloat(normal_format($(this).children(".depreciation").val()));
                         ie=parseFloat(normal_format($(this).children(".interestexpenses").val()));
                         ii=parseFloat(normal_format($(this).children(".interestincome").val()));
                         it=parseFloat(normal_format($(this).children(".incometax").val()));

                         $.ajax({
                              type: "POST",
                              dataType: 'json',
                              async: true,
                              cache: false,
                             url: "/EvalCompanyDashboards/buttonclk/"+useronline,
                             data:
                               {

                                     "buttonstates":buttonstate,
                                     "totalrevenue":tr,
                                     "materialexp":me,
                                     "personalexp":pe,
                                     "otherexp":oe,
                                     "otherincome":oi,
                                     "depreciation":dep,
                                     "interestexp":ie,
                                     "interestincome":ii,
                                     "incometaxes":it,
                               }
                         })
                         }
                       })

  }
 })



 $("#register").click(function(){
buttonstate="on";
 tr=parseFloat(normal_format( $(this).children("#pertotalrevenue").val()));
 me=parseFloat(normal_format( $(this).children(".permaterialexp").val()));
 pe=parseFloat(normal_format( $(this).children(".perpersonalexp").val()));
 oe=parseFloat(normal_format( $(this).children(".perotherexp").val()));
 oi=parseFloat(normal_format( $(this).children(".perotherincome").val()));
 dep=parseFloat(normal_format( $(this).children(".perdepreciation").val()));
 ie=parseFloat(normal_format( $(this).children(".perinterestexp").val()));
 ii=parseFloat(normal_format( $(this).children(".perinterestincome").val()));
 it=parseFloat(normal_format( $(this).children(".incometaxes").val()));
                         $.ajax({
                              type: "POST",
                              dataType: 'json',
                              async: true,
                              cache: false,
                             url: "/firstregistration",
                             data:
                               {

                                     "autopriceincrement":buttonstate,
                                     "pertotalrevenue":tr,
                                     "permaterialexp":me,
                                     "perpersonalexp":pe,
                                     "perotherexp":oe,
                                     "perotherincome":oi,
                                     "perdepreciation":dep,
                                     "perinterestexp":ie,
                                     "perinterestincome":ii,
                                     "incometaxes":it,
                               }
                         })

});







$("#submit").click(function(){

       var valyear=0,useronline=0,valrevenue=0,valmaterialexpenses=0,valpersonnelexpenses=0,valotherexpenses=0,valotherincome=0,valebitda=0,valebitdamargin=0,valdepreciation=0,valebit=0,valebitmargin=0,valinterestexpenses=0,valinterestincome=0,valebt=0,valincometax=0,valnetincome=0;
       var valfixedasset=0,valfinanacialasset=0,valinventories=0,valtrade=0,valothercurrentassets=0,valcash=0,valtotalcurrentassets=0,valtotalassets=0,valequity=0,valfinancialdebt=0,valpensionprovisions=0,valotherprovisions=0,valtradepayables=0,valotherliabilities=0,valtotalliabilities=0,valtotalequity=0,valcapitalexpendituresinfixedasset=0,valcapitalexpenditurepercent=0,valnetcurrentassets =0,valnetcurrentassetspercent=0;

       $("div").each(function(){
       debugger;
       alert($(this).attr("class"));

               if($(this).attr("class").indexOf("year")!=-1)
                         {

                     valyear=$(this).attr("class");
                   //  useronline="{{currentuser}}";
                     valrevenue=parseFloat(normal_format( $(this).children(".revenues").val()));
                     valmaterialexpenses=parseFloat(normal_format( $(this).children(".materialexpenses").val()));
                     valpersonnelexpenses=parseFloat(normal_format( $(this).children(".personnelexpenses").val()));
                     valotherexpenses=parseFloat(normal_format( $(this).children(".otherexpenses").val()));
                     valotherincome=parseFloat(normal_format( $(this).children(".otherincome").val()));
                     valebitda=parseFloat(normal_format( $(this).children(".ebitda").val()));
                     valebitdamargin=parseFloat(( $(this).children(".ebitdamargin").val()));
                     valdepreciation=parseFloat(normal_format( $(this).children(".depreciation").val()));
                     valebit=parseFloat(normal_format( $(this).children(".ebit").val()));
                     valebitmargin=parseFloat( $(this).children(".ebitmargin").val());
                     valinterestexpenses=parseFloat(normal_format( $(this).children(".interestexpenses").val()));
                     valinterestincome=parseFloat(normal_format( $(this).children(".interestincome").val()));
                     valebt=parseFloat(normal_format( $(this).children(".ebt").val()));
                     valincometax=parseFloat(normal_format( $(this).children(".incometax").val()));
                     valnetincome=parseFloat(normal_format( $(this).children(".netincome").val()));
                     valfixedasset=parseFloat(normal_format( $(this).children(".fixedasset").val()));
                     valfinanacialasset=parseFloat(normal_format( $(this).children(".finanacialasset").val()));
                     valinventories=parseFloat(normal_format( $(this).children(".inventories").val()));
                     valtrade=parseFloat(normal_format( $(this).children(".trade").val()));
                     valothercurrentassets=parseFloat(normal_format( $(this).children(".othercurrentassets").val()));
                     valcash=parseFloat(normal_format( $(this).children(".cash").val()));
                     valtotalcurrentassets=parseFloat(( $(this).children(".totalcurrentassets").val()));
                     valtotalassets=parseFloat(normal_format( $(this).children(".totalassets").val()));
                     valequity=parseFloat(normal_format( $(this).children(".equity").val()));
                     valfinancialdebt=parseFloat( $(this).children(".financialdebt").val());
                     valpensionprovisions=parseFloat(normal_format( $(this).children(".pensionprovisions").val()));
                     valotherprovisions=parseFloat(normal_format( $(this).children(".otherprovisions").val()));
                     valtradepayables=parseFloat(normal_format( $(this).children(".tradepayables").val()));
                     valotherliabilities=parseFloat(normal_format( $(this).children(".otherliabilities").val()));
                     valtotalliabilities=parseFloat(normal_format( $(this).children(".totalliabilities").val()));
                     valtotalequity=parseFloat(normal_format( $(this).children(".totalequity").val()));
                     valcapitalexpendituresinfixedasset=parseFloat(normal_format( $(this).children(".capitalexpendituresinfixedasset").val()));
                     valcapitalexpenditurepercent=parseFloat( $(this).children(".capitalexpenditurepercent").val());
                     valnetcurrentassets=parseFloat(normal_format( $(this).children(".netcurrentassets").val()));
                     valnetcurrentassetspercent=parseFloat($(this).children(".netcurrentassetspercent").val());
                     //useronline="{{currentuser}}";
                     alert("useronline");
                           $.ajax({
                                   type: "POST",
                                   dataType: 'json',
                                   async: true,
                                   cache: false,

                                   url: "/EvalCompanyDashboard/datasubmit/",
                                   data:
                                   {
                      "nowuser":"useronline",
                      "years":valyear,
                      "revenue":valrevenue,
                      "materialexpenses":valmaterialexpenses,
                      "personnelexpenses":valpersonnelexpenses,
                      "otherexpenses":valotherexpenses,
                      "otherincome":valotherincome,
                      "ebitda":valebitda,
                      "ebitdamargin":valebitdamargin,
                      "depreciation":valdepreciation,
                      "ebit":valebit,
                      "ebitmargin":valebitmargin,
                      "interestexpenses":valinterestexpenses,
                      "interestincome":valinterestincome,
                      "ebt":valebt,
                      "incometax":valincometax,
                      "netincome":valnetincome,

                     "fixedasset":valfixedasset,
                     "finanacialasset":valfinanacialasset,
                     "inventories":valinventories,
                     "trade":valtrade,
                     "othercurrentassets":valothercurrentassets,
                     "cash":valcash,
                     "totalcurrentassets":valtotalcurrentassets,
                     "totalassets":valtotalassets,
                     "equity":valequity,
                     "financialdebt":valfinancialdebt,
                     "pensionprovisions":valpensionprovisions,
                     "otherprovisions":valotherprovisions,
                     "tradepayables":valtradepayables,
                     "otherliabilities":valotherliabilities,
                     "totalliabilities":valtotalliabilities,
                     "totalequity":valtotalequity,

                     "capitalexpendituresinfixedasset":valcapitalexpendituresinfixedasset,
                     "capitalexpenditurepercent":valcapitalexpenditurepercent,
                     "netcurrentassets":valnetcurrentassets,
                     "netcurrentassetspercent":valnetcurrentassetspercent,
                     }
             });
          }
       });
});

})
