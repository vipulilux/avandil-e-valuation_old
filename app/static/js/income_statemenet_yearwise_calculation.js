    $(document).ready(function(){

       $(".txt,.txt1,.txt2").keyup(function(){
                   $(this).val(german_format(normal_format($(this).val())));

        });





       $(".txt,.txt1,.txt2").change(function() {
         var incometaxs=0,interestincomes=0,interestexp=0,ebitval=0,rev=0,matexp=0,otherincomes=0,persexp=0,otherexp=0,otherincomes=0,ebitdaval=0,depreciationval=0,depreciationval=0;
         var incometaxsperc=0,interestincomesperc=0,interestexpperc=0,ebitperc=0,revperc=0,matexpperc=0,persexpperc=0,otherexpperc=0,otherincomesperc=0,otherincomesperc=0,ebitdaperc=0,depreciationperc=0,depreciationperc=0;

	      if(!isNaN(this.value)&& this.value.length!=0 ) {

          if($(this).attr("class").includes("revenues")){
                  rev=parseFloat(normal_format($(this).val()));
           }

          else
          {
                   rev=parseFloat(normal_format($(this).siblings(".revenues").val()));
          }

          if(!rev )
          { rev=0;}

          revperc=parseFloat((rev/rev)*100).toFixed(2);
          if(isNaN(revperc)){
                revperc = 0;
           }

          if($(this).attr("class").includes("materialexpenses"))
           {
                  matexp=parseFloat(normal_format($(this).val()));
           }
           else
           {
                 matexp=parseFloat(normal_format($(this).siblings(".materialexpenses").val()));
           }
         if(!matexp )
          { matexp=0;}
         matexpperc=parseFloat((matexp/rev)*100).toFixed(2);
         if(isNaN(matexpperc)){
                 matexpperc = 0;
            }

         if($(this).attr("class").includes("personnelexpenses"))
           {
            persexp=parseFloat(normal_format($(this).val()));
           }
            else
           {
           persexp=parseFloat(normal_format($(this).siblings(".personnelexpenses").val()));
          }


         if(!persexp )
        { persexp=0;}
        persexpperc=parseFloat((persexp/rev)*100).toFixed(2);
        if(isNaN(persexpperc)){
          persexpperc = 0;
         }

       if($(this).attr("class").includes("otherexpenses"))
        {
          otherexp=parseFloat(normal_format($(this).val()));
        }
        else
        {
         otherexp=parseFloat(normal_format($(this).siblings(".otherexpenses").val()));
        }

       if(isNaN(otherexp))
        { otherexp=0;}
        otherexpperc=parseFloat((otherexp/rev)*100).toFixed(2);
        if(isNaN(otherexpperc)){
           otherexpperc = 0;
         }

       if($(this).attr("class").includes("otherincome"))
        {
           otherincomes=parseFloat(normal_format($(this).val()));
        }
       else
        {
           otherincomes=parseFloat(normal_format($(this).siblings(".otherincome").val()));
         }
       if(!otherincomes )
        { otherincomes=0;}
       otherincomesperc=parseFloat((otherincomes/rev)*100).toFixed(2);
       if(isNaN(otherincomesperc)){
         otherincomesperc = 0;
        }

       if($(this).attr("class").includes("depreciation"))
       {
         depreciationval=parseFloat(normal_format($(this).val()));
        }
       else
        {
           depreciationval=parseFloat(normal_format($(this).siblings(".depreciation").val()));
        }
       if(!depreciationval)
       { depreciationval=0;}
        depreciationperc=parseFloat((depreciationval/rev)*100).toFixed(2);
       if(isNaN(depreciationperc)){
       depreciationperc = 0;
       }

       if($(this).attr("class").includes("interestexpenses"))
       {
         interestexp=parseFloat(normal_format($(this).val()));
       }
       else
       {
         interestexp=parseFloat(normal_format($(this).siblings(".interestexpenses").val()));
       }
        if(!interestexp)
        { interestexp=0;}

       interestexpperc=parseFloat((interestexp/rev)*100).toFixed(2);
       if(isNaN(interestexpperc)){
         interestexpperc = 0;
        }

       if($(this).attr("class").includes("interestincome"))
       {
        interestincomes=parseFloat(normal_format($(this).val()));
       }
       else
        {
          interestincomes=parseFloat(normal_format($(this).siblings(".interestincome").val()));
        }
        if(!interestincomes)
         { interestincomes=0;}

        interestincomesperc=parseFloat((interestincomes/rev)*100).toFixed(2);

        if(isNaN(interestincomesperc)){
         interestincomesperc = 0;
        }


        if($(this).attr("class").includes("incometax"))
        {
         incometaxs=parseFloat(normal_format($(this).val()));
        }
        else
         {
           incometaxs=parseFloat(normal_format($(this).siblings(".incometax").val()));
         }
        if(!incometaxs)
         { incometaxs=0;}

       incometaxsperc=parseFloat((incometaxs/rev)*100).toFixed(2);
        if(isNaN(incometaxsperc)){
         incometaxsperc = 0;
        }

        var valebitda=(rev+otherincomes)-(matexp+ persexp + otherexp);
        var   ebitdaperc=parseFloat((valebitda/rev)*100).toFixed(2);
        if(isNaN(ebitdaperc)){
               ebitdaperc = 0;
         }
        var valebitdamargin=parseFloat((valebitda/rev)*100).toFixed(2);
        if(isNaN(valebitdamargin)){
        valebitdamargin = 0;
       }


       var valebit=parseFloat(valebitda-depreciationval);

       if(isNaN(ebitperc)){
       ebitperc = 0;
       }

       var valebitmargin=parseFloat((valebit/rev)*100).toFixed(2);

       if(isNaN(valebitmargin)){
       valebitmargin = 0;
       }

         var valebt=parseFloat((valebit-interestexp)+interestincomes);
         var valnetincome=parseFloat(valebt-incometaxs);
         var pernetincome=parseFloat((valnetincome/rev)*100);

        $(this).siblings(".ebitda").val(german_format(valebitda));
        $(this).siblings(".ebitdamargin").val(valebitdamargin);
        $(this).siblings(".ebit").val(german_format(valebit));
        $(this).siblings(".ebitmargin").val(valebitmargin);
        $(this).siblings(".ebt").val(german_format(valebt));
        $(this).siblings(".netincome").val(german_format(valnetincome));

        if(revperc>=100)
        {
        $(this).siblings(".revenuesper").html("&nbsp;"+revperc+"%");
        }
        else
        {
        $(this).siblings(".revenuesper").html("&nbsp;"+revperc+"%");
        }

        $(this).siblings(".materialexpensesper").html("&nbsp;"+matexpperc+"%");
        $(this).siblings(".personnelexpensesper").html("&nbsp;"+persexpperc+"%");
        $(this).siblings(".otherexpensesper").html("&nbsp;"+otherexpperc+"%");
        $(this).siblings(".otherincomeper").html("&nbsp;"+otherincomesperc+"%");
        $(this).siblings(".depreciationper").html("&nbsp;"+depreciationperc+"%");
        $(this).siblings(".interestexpensesper").html("&nbsp;"+interestexpperc+"%");
        $(this).siblings(".interestincomeper").html("&nbsp;"+interestincomesperc+"%");
        $(this).siblings(".incometaxper").html("&nbsp;"+incometaxsperc+"%");
        $(this).siblings(".netincomeper").html("&nbsp;"+pernetincome+"%");
       // $(".fixedtxt").css("width", "255px");
       // $(".fixedtxt").css("margin-right", "02px");


       }
    });
})