   {% if avansize==0  %}
<script type="text/javascript">
    $(document).ready(function(){
    //button status on
    buttonstate="{{buttononoff}}";


       if(buttonstate=="on")
         {
           $('#activeSwitch').prop('checked', true).change();
           var yrplus=0;
           //searching for div tag
          $("div").each(function(){
          //searching for year in class

          if($(this).attr("class").indexOf("income_statement year"+(new Date().getFullYear()+yrplus))!=-1)
            {
               var yr=0;
               yr =$(this).attr("class").slice(30,);
               var currentTime = new Date();
               yrplus=yrplus+1;

              //display value in current year
              if (yr == new Date().getFullYear())
             {
              var totalrev=parseFloat("{{companydata[0].totalrevenue}}");

             var rev=parseFloat("{{companydata[0].totalrevenue}}");
             $(this).children(".revenues").val(german_format("{{companydata[0].totalrevenue}}"));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");

             $(this).children(".materialexpenses").val(german_format("{{companydata[0].materialexp}}"));
              var materialexpenses=parseFloat("{{companydata[0].materialexp}}");
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format("{{companydata[0].personalexp}}"));
             var personnelexpenses=parseFloat("{{companydata[0].personalexp}}");
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format("{{companydata[0].otherexp}}"));

             var otherexpenses=parseFloat("{{companydata[0].otherexp}}");
             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format("{{companydata[0].otherincome}}"));

             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitda").val(german_format("{{companydata[0].ebitda}}"));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");

             $(this).children(".depreciation").val(german_format("{{companydata[0].depreciation}}"));
              var depreciation=parseFloat("{{companydata[0].depreciation}}");
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             $(this).children(".ebt").val(german_format("{{companydata[0].ebt}}"));

              var ebt=parseFloat("{{companydata[0].ebt}}");
             $(this).children(".ebtper").html("&nbsp;"+((ebt/rev)*100).toFixed(2)+"%");
             $(this).children(".incometax").val(german_format("{{companydata[0].incometaxes}}"));

              var incometax=parseFloat("{{companydata[0].incometaxes}}");
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format("{{companydata[0].netincome}}"));

              var netincome=parseFloat("{{companydata[0].netincome}}");
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(german_format("{{companydata[0].ebitdamargin}}"));

              var ebitdamargin=parseFloat("{{companydata[0].ebitdamargin}}");
             $(this).children(".ebitdamarginper").html("&nbsp;"+((ebitdamargin/rev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format("{{companydata[0].ebit}}"));

              var ebit=parseFloat("{{companydata[0].ebit}}");
             $(this).children(".ebitper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");
             $(this).children(".interestexpenses").val(german_format("{{companydata[0].interestexp}}"));

              var interestexpenses=parseFloat("{{companydata[0].interestexp}}");
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".interestincome").val(german_format("{{companydata[0].interestincome}}"));
             var interestincome=parseFloat("{{companydata[0].interestincome}}");
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitmargin").val(german_format("{{companydata[0].ebitmargin}}"));
             var ebitmargin=parseFloat("{{companydata[0].ebitmargin}}");
             $(this).children(".ebitmarginper").html("&nbsp;"+((ebitmargin/rev)*100).toFixed(2)+"%");

              ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
             $(this).children(".ebitda").val(german_format(ebitdaval));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");

             ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
             $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format(ebitval));
             $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
             ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

             $(this).children(".ebt").val(german_format(ebtval));
             $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

              netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             //display value in next years
             else
             {

             var rev=0;
             var mexp=0;
             var pexp=0;
             var oexp=0;
             var oincome=0;
             var dep=0;
             var intexp=0;
             var intintincome=0;
             var intax=0;
             if ($(this).prev().children(".revenues").length==0)
             {
              rev=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".revenues").val()));

             }
             else
             {

              rev=parseFloat(normal_format($(this).prev().children(".revenues").val()));
             }
            totalrev=(rev*(parseFloat("{{companydata[0].pertotalrevenue}}")+100))/100;
              $(this).children(".revenues").val(german_format(totalrev));
             $(this).children(".revenuesper").html("&nbsp;"+((totalrev/totalrev)*100).toFixed(2)+"%");

              if ($(this).prev().children(".materialexpenses").length==0)
             {
              mexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".materialexpenses").val()));

             }
             else
             {

              mexp=parseFloat(normal_format($(this).prev().children(".materialexpenses").val()));
             }

            totalmexp=(mexp*(parseFloat("{{companydata[0].permaterialexp}}")+100))/100;
              $(this).children(".materialexpenses").val(german_format(totalmexp));
             $(this).children(".materialexpensesper").html("&nbsp;"+((totalmexp/totalrev)*100).toFixed(2)+"%");

             if ($(this).prev().children(".personnelexpenses").length==0)
             {
              pexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".personnelexpenses").val()));

             }
             else
             {

              pexp=parseFloat(normal_format($(this).prev().children(".personnelexpenses").val()));
             }

            totalpexp=(pexp*(parseFloat("{{companydata[0].perpersonalexp}}")+100))/100;
              $(this).children(".personnelexpenses").val(german_format(totalmexp));
             $(this).children(".personnelexpensesper").html("&nbsp;"+((totalpexp/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".otherexpenses").length==0)
             {
              oexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".otherexpenses").val()));

             }
             else
             {

              oexp=parseFloat(normal_format($(this).prev().children(".otherexpenses").val()));
             }

            totaloexp=(pexp*(parseFloat("{{companydata[0].perotherexp}}")+100))/100;
              $(this).children(".otherexpenses").val(german_format(totaloexp));
             $(this).children(".otherexpensesper").html("&nbsp;"+((totaloexp/totalrev)*100).toFixed(2)+"%");


             if ($(this).prev().children(".otherincome").length==0)
             {
              oincome=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".otherincome").val()));

             }
             else
             {

              oincome=parseFloat(normal_format($(this).prev().children(".otherincome").val()));
             }

            totaloincome=(pexp*(parseFloat("{{companydata[0].perotherincome}}")+100))/100;
              $(this).children(".otherincome").val(german_format(totaloincome));
             $(this).children(".otherincomeper").html("&nbsp;"+((totaloincome/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".depreciation").length==0)
             {
              dep=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".depreciation").val()));

             }
             else
             {

              dep=parseFloat(normal_format($(this).prev().children(".depreciation").val()));
             }

            totaldep=(dep*(parseFloat("{{companydata[0].perdepreciation}}")+100))/100;
              $(this).children(".depreciation").val(german_format(totaldep));
             $(this).children(".depreciationper").html("&nbsp;"+((totaldep/totalrev)*100).toFixed(2)+"%");


             if ($(this).prev().children(".interestexpenses").length==0)
             {
              intexp=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".interestexpenses").val()));

             }
             else
             {

              intexp=parseFloat(normal_format($(this).prev().children(".interestexpenses").val()));
             }

            totalintexp=(intexp*(parseFloat("{{companydata[0].perinterestexp}}")+100))/100;
              $(this).children(".interestexpenses").val(german_format(totalintexp));
             $(this).children(".interestexpensesper").html("&nbsp;"+((totalintexp/totalrev)*100).toFixed(2)+"%");



             if ($(this).prev().children(".interestincome").length==0)
             {
              intintincome=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".interestincome").val()));

             }
             else
             {

              intintincome=parseFloat(normal_format($(this).prev().children(".interestincome").val()));
             }

            totalintincome=(intexp*(parseFloat("{{companydata[0].perinterestincome}}")+100))/100;
              $(this).children(".interestincome").val(german_format(totalintincome));
             $(this).children(".interestincomeper").html("&nbsp;"+((totalintincome/totalrev)*100).toFixed(2)+"%");


             if ($(this).prev().children(".incometax").length==0)
             {
              intax=parseFloat(normal_format($(this).parent().prev().children().next().next().children(".incometax").val()));

             }
             else
             {

              intax=parseFloat(normal_format($(this).prev().children(".incometax").val()));
             }

            totalintax=(intexp*(parseFloat("{{companydata[0].incometaxes}}")+100))/100;
              $(this).children(".incometax").val(german_format(totalintax));
             $(this).children(".incometaxper").html("&nbsp;"+((totalintax/totalrev)*100).toFixed(2)+"%");
           //  alert($(this).children(".revenues").val());

             ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
             $(this).children(".ebitda").val(german_format(ebitdaval));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");

             ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
             $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format(ebitval));
             $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
             ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

             $(this).children(".ebt").val(german_format(ebtval));
             $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

              netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             }
             });
       }
       //button status is off
        else
        {
          $('#activeSwitch').prop('checked', false).change();
          var yrplus=0;

         $("div").each(function(){

          if($(this).attr("class").indexOf("income_statement year"+(new Date().getFullYear()+yrplus))!=-1)
            {

               var yr=0;
             yr =$(this).attr("class").slice(30,);
             var currentTime = new Date();
            // alert(new Date().getFullYear()+yrplus);
             yrplus=yrplus+1;

              if (yr == new Date().getFullYear())
             {
             var totalrev=parseFloat("{{companydata[0].totalrevenue}}");

             var rev=parseFloat("{{companydata[0].totalrevenue}}");
             $(this).children(".revenues").val(german_format("{{companydata[0].totalrevenue}}"));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");

             $(this).children(".materialexpenses").val(german_format("{{companydata[0].materialexp}}"));
              var materialexpenses=parseFloat("{{companydata[0].materialexp}}");
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format("{{companydata[0].personalexp}}"));
             var personnelexpenses=parseFloat("{{companydata[0].personalexp}}");
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format("{{companydata[0].otherexp}}"));

             var otherexpenses=parseFloat("{{companydata[0].otherexp}}");



             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format("{{companydata[0].otherincome}}"));

             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitda").val(german_format("{{companydata[0].ebitda}}"));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");

             $(this).children(".depreciation").val(german_format("{{companydata[0].depreciation}}"));
              var depreciation=parseFloat("{{companydata[0].depreciation}}");
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             $(this).children(".ebt").val(german_format("{{companydata[0].ebt}}"));

              var ebt=parseFloat("{{companydata[0].ebt}}");
             $(this).children(".ebtper").html("&nbsp;"+((ebt/rev)*100).toFixed(2)+"%");
             $(this).children(".incometax").val(german_format("{{companydata[0].incometaxes}}"));

              var incometax=parseFloat("{{companydata[0].incometaxes}}");
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format("{{companydata[0].netincome}}"));

              var netincome=parseFloat("{{companydata[0].netincome}}");
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(german_format("{{companydata[0].ebitdamargin}}"));

              var ebitdamargin=parseFloat("{{companydata[0].ebitdamargin}}");
             $(this).children(".ebitdamarginper").html("&nbsp;"+((ebitdamargin/rev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format("{{companydata[0].ebit}}"));

              var ebit=parseFloat("{{companydata[0].ebit}}");
             $(this).children(".ebitper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");
             $(this).children(".interestexpenses").val(german_format("{{companydata[0].interestexp}}"));

              var interestexpenses=parseFloat("{{companydata[0].interestexp}}");
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".interestincome").val(german_format("{{companydata[0].interestincome}}"));
             var interestincome=parseFloat("{{companydata[0].interestincome}}");
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitmargin").val(german_format("{{companydata[0].ebitmargin}}"));
             var ebitmargin=parseFloat("{{companydata[0].ebitmargin}}");
             $(this).children(".ebitmarginper").html("&nbsp;"+((ebitmargin/rev)*100).toFixed(2)+"%");

              ebitdaval=(parseFloat(normal_format($(this).children(".revenues").val()))+parseFloat(normal_format($(this).children(".otherincome").val())))-(parseFloat(normal_format($(this).children(".otherexpenses").val()))+parseFloat(normal_format($(this).children(".personnelexpenses").val()))+parseFloat(normal_format($(this).children(".materialexpenses").val())));
             $(this).children(".ebitda").val(german_format(ebitdaval));
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitdaval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(((ebitdaval/totalrev)*100).toFixed(2)+"%");

             ebitval=ebitdaval-parseFloat(normal_format($(this).children(".depreciation").val()));
             $(this).children(".ebitper").html("&nbsp;"+((ebitval/totalrev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format(ebitval));
             $(this).children(".ebitmargin").val(((ebitval/totalrev)*100).toFixed(2)+"%");
             ebtval=ebitval-parseFloat(normal_format($(this).children(".interestexpenses").val()))+parseFloat(normal_format($(this).children(".interestincome").val()))

             $(this).children(".ebt").val(german_format(ebtval));
             $(this).children(".ebtper").html("&nbsp;"+((ebtval/totalrev)*100).toFixed(2)+"%");

              netloss=ebtval-parseFloat(normal_format($(this).children(".incometax").val()));
             $(this).children(".netincomeper").html("&nbsp;"+((netloss/totalrev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format(netloss));

             }
             else
             {
             $(this).children(".revenues").val(german_format(0));
             $(this).children(".revenuesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".materialexpenses").val(german_format(0));
              $(this).children(".materialexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".personnelexpenses").val(german_format(0));
             $(this).children(".personnelexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherexpenses").val(german_format(0));
             $(this).children(".otherexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".otherincome").val(german_format(0));
             var otherincome=parseFloat("{{companydata[0].otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
              var ebitda=parseFloat("{{companydata[0].ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");

             $(this).children(".depreciation").val(german_format(0));
             $(this).children(".depreciationper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".incometax").val(german_format(0));
             $(this).children(".incometaxper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincome").val(german_format(0));
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitdamargin").val(german_format(0));
             $(this).children(".ebitdamarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
             $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestexpenses").val(german_format(0));
             $(this).children(".interestexpensesper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".interestincome").val(german_format(0));

             $(this).children(".interestincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitmargin").val(german_format(0));
             $(this).children(".ebitmarginper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitda").val(german_format(0));
             $(this).children(".ebitdaper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebitdamargin").val("&nbsp;"+"0.00"+"%");
              $(this).children(".ebitper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".ebit").val(german_format(0));
             $(this).children(".ebitmargin").val("&nbsp;"+"0.00"+"%");
              $(this).children(".ebt").val(german_format(0));
             $(this).children(".ebtper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincomeper").html("&nbsp;"+"0.00"+"%");
             $(this).children(".netincome").val("&nbsp;"+"0.00"+"%");

             }
             }
    })
    }
    })
    </script>
      {% endif %}

    {% if  avansize > 0 %}
    {% for each1 in userdata %}
<script type="text/javascript">
        $(document).ready(function(){
        debugger;
        //alert("{{each1.revenues}}");
        //alert("{{companydata[0].totalrevenue}}");

        buttonstate="{{buttononoff}}";
        alert(buttonstate);
        if (buttonstate == 'off') {
        $('#activeSwitch').prop('checked', false).change();

         }else
         {
         $('#activeSwitch').prop('checked', true).change();
         }




          $("div").each(function(){


            if($(this).attr("class").indexOf("year")!=-1)
            {
             var yr=0;
             if($(this).attr("class").indexOf("income_statement")!=-1)
             yr =$(this).attr("class").slice(30,);

             if($(this).attr("class").indexOf("balance_sheet")!=-1)
             yr =$(this).attr("class").slice(27,);
             if($(this).attr("class").indexOf("investment")!=-1)
             yr =$(this).attr("class").slice(24,);

             if (yr == "{{each1.year}}")
             {

             var rev=parseFloat("{{each1.revenues}}");
             $(this).children(".revenues").val(german_format("{{each1.revenues}}"));
             $(this).children(".revenuesper").html("&nbsp;"+((rev/rev)*100).toFixed(2)+"%");

             $(this).children(".materialexpenses").val(german_format("{{each1.materialexpenses}}"));
              var materialexpenses=parseFloat("{{each1.materialexpenses}}");
              $(this).children(".materialexpensesper").html("&nbsp;"+((materialexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".personnelexpenses").val(german_format("{{each1.personnelexpenses}}"));
             var personnelexpenses=parseFloat("{{each1.personnelexpenses}}");
             $(this).children(".personnelexpensesper").html("&nbsp;"+((personnelexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherexpenses").val(german_format("{{each1.otherexpenses}}"));

             var otherexpenses=parseFloat("{{each1.otherexpenses}}");
             $(this).children(".otherexpensesper").html("&nbsp;"+((otherexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".otherincome").val(german_format("{{each1.otherincome}}"));

             var otherincome=parseFloat("{{each1.otherincome}}");
             $(this).children(".otherincomeper").html("&nbsp;"+((otherincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitda").val(german_format("{{each1.ebitda}}"));
              var ebitda=parseFloat("{{each1.ebitda}}");
             $(this).children(".ebitdaper").html("&nbsp;"+((ebitda/rev)*100).toFixed(2)+"%");

             $(this).children(".depreciation").val(german_format("{{each1.depreciation}}"));
              var depreciation=parseFloat("{{each1.depreciation}}");
             $(this).children(".depreciationper").html("&nbsp;"+((depreciation/rev)*100).toFixed(2)+"%");
             $(this).children(".ebt").val(german_format("{{each1.ebt}}"));

              var ebt=parseFloat("{{each1.ebt}}");
             $(this).children(".ebtper").html("&nbsp;"+((ebt/rev)*100).toFixed(2)+"%");
             $(this).children(".incometax").val(german_format("{{each1.incometax}}"));

              var incometax=parseFloat("{{each1.incometax}}");
             $(this).children(".incometaxper").html("&nbsp;"+((incometax/rev)*100).toFixed(2)+"%");
             $(this).children(".netincome").val(german_format("{{each1.netincome}}"));

              var netincome=parseFloat("{{each1.netincome}}");
             $(this).children(".netincomeper").html("&nbsp;"+((netincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitdamargin").val(german_format("{{each1.ebitdamargin}}"));

              var ebitdamargin=parseFloat("{{each1.ebitdamargin}}");
             $(this).children(".ebitdamarginper").html("&nbsp;"+((ebitdamargin/rev)*100).toFixed(2)+"%");
             $(this).children(".ebit").val(german_format("{{each1.ebit}}"));

              var ebit=parseFloat("{{each1.ebit}}");
             $(this).children(".ebitper").html("&nbsp;"+((ebit/rev)*100).toFixed(2)+"%");
             $(this).children(".interestexpenses").val(german_format("{{each1.interestexpenses}}"));

              var interestexpenses=parseFloat("{{each1.interestexpenses}}");
             $(this).children(".interestexpensesper").html("&nbsp;"+((interestexpenses/rev)*100).toFixed(2)+"%");
             $(this).children(".interestincome").val(german_format("{{each1.interestincome}}"));
             var interestincome=parseFloat("{{each1.interestincome}}");
             $(this).children(".interestincomeper").html("&nbsp;"+((interestincome/rev)*100).toFixed(2)+"%");
             $(this).children(".ebitmargin").val(german_format("{{each1.ebitmargin}}"));
             var ebitmargin=parseFloat("{{each1.ebitmargin}}");
             $(this).children(".ebitmarginper").html("&nbsp;"+((ebitmargin/rev)*100).toFixed(2)+"%");



             $(this).children(".fixedasset").val(german_format("{{each1.fixedasset}}"));
             $(this).children(".finanacialasset").val(german_format("{{each1.finanacialasset}}"));
             $(this).children(".inventories").val(german_format("{{each1.inventories}}"));
             $(this).children(".trade").val(german_format("{{each1.trade}}"));
             $(this).children(".othercurrentassets").val(german_format("{{each1.othercurrentassets}}"));
             $(this).children(".cash").val(german_format("{{each1.cash}}"));
             $(this).children(".totalcurrentassets").val(german_format("{{each1.totalcurrentassets}}"));
             $(this).children(".totalassets").val(german_format("{{each1.totalassets}}"));
             $(this).children(".equity").val(german_format("{{each1.equity}}"));
             $(this).children(".financialdebt").val(german_format("{{each1.financialdebt}}"));
             $(this).children(".pensionprovisions").val(german_format("{{each1.pensionprovisions}}"));
             $(this).children(".otherprovisions").val(german_format("{{each1.otherprovisions}}"));
             $(this).children(".tradepayables").val(german_format("{{each1.tradepayables}}"));
             $(this).children(".otherliabilities").val(german_format("{{each1.otherliabilities}}"));
             $(this).children(".totalliabilities").val(german_format("{{each1.totalliabilities}}"));
             $(this).children(".totalequity").val(german_format("{{each1.totalequity}}"));
             $(this).children(".capitalexpendituresinfixedasset").val(german_format("{{each1.capitalexpendituresinfixedasset}}"));
             $(this).children(".capitalexpenditurepercent").val(german_format("{{each1.capitalexpenditurepercent}}"));
             $(this).children(".netcurrentassets").val(german_format("{{each1.netcurrentassets}}"));
             $(this).children(".netcurrentassetspercent").val(german_format("{{each1.netcurrentassetspercent}}"));
             }
             }
            });

            })
            </script>
{% endfor %}
{% endif %}
