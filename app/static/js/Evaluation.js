 function german_format(num){
                 var k="";
            if (!num){
            num=0;
            }
            var a=num.toString();
            if (a.indexOf(".") != -1){
            var k = a.slice(a.indexOf(".")+1,a.indexOf(".")+4);
            var a = a.slice(0,a.indexOf("."));
            }
            var i=a.length;
            var b="";
            var c=0;
            while(i--){
            if(c%3==0 && c != 0){
            b=b+".";
            }
            c=parseFloat(c)+1;
            b=b+a.charAt(i);
            }
            final_ans = b.split("").reverse().join("");
            return final_ans;
  }
  function normal_format(num){
            if (!num){
            num=0;
            }
            var a=num.toString();

            while(a.indexOf(".") != -1){
            a=a.replace(".","");
          }
          while(a.indexOf(",") != -1){
          a=a.replace(",",".");
          }
          return a;
         }



function auto_calculate_incomestm(fieldvalue,incrementpercent)
{
return(parseFloat(fieldvalue)*parseFloat(incrementpercent));
}




function ebitda_calculate(rev,me,pe,oe,oi)
{
return((parseFloat(rev)+parseFloat(oi))-(parseFloat(me)+parseFloat(pe)+parseFloat(oe)));
}



function ebit_calculate(ebitda,dep)
{
return((parseFloat(ebitda))-(parseFloat(dep)));
}


function ebt_calculate(ebit,ie,ii)
{
return((parseFloat(ebit)+parseFloat(ii))-(parseFloat(ie)));
}

function netloss_calculate(ebt,it)
{
return(parseFloat(ebt)-parseFloat(it));
}

function Total_Current_Assets_calculate(valinventories,valothercurrentassets,valcash,valtrade)
{
return(parseFloat(valinventories)+parseFloat(valothercurrentassets)+parseFloat(valcash)+parseFloat(valtrade));
}


function Current_Assets_calculate(valinventories,valothercurrentassets,valcash,valtrade)
{
return(parseFloat(valinventories)+parseFloat(valothercurrentassets)+parseFloat(valcash)+parseFloat(valtrade)+parseFloat(valfixedasset));
}


function Total_Liabilities_calculate(valfinancialdebt,valpensionprovisions,valotherprovisions,valtradepayables,valotherliabilities)
{
return(parseFloat(valfinancialdebt)+parseFloat(valpensionprovisions)+parseFloat(valotherprovisions)+parseFloat(valtradepayables)+parseFloat(valotherliabilities));
}


function Total_Equity_Liabilities_calculate(valfinancialdebt,valpensionprovisions,valotherprovisions,valtradepayables,valotherliabilities)
{
return(parseFloat(valequity)+parseFloat(valfinancialdebt)+parseFloat(valpensionprovisions)+parseFloat(valotherprovisions)+parseFloat(valtradepayables)+parseFloat(valotherliabilities));
}

function DOS_calculate(inventory,revenue)
{
return((parseFloat(inventory)*365)/parseFloat(revenue));
}

function DSO_calculate(trade,revenue)
{
return((parseFloat(trade)*365)/parseFloat(revenue));
}



function Fixed_assets_calculate(fixedassets,depreciation,lastyrfixedasset,avgperofcoefa)
{
//excel sheet formula
return((parseFloat(fixedassets)-parseFloat(depreciation))+((parseFloat(lastyrfixedasset)*parseFloat(avgperofcoefa))/100));
}

function inventories_calculate(dos,revenue)
{
//given formula
return((parseFloat(dos)*parseFloat(revenue))/365);
}

function Trade_receivable_calculate(dso,revenue)
{
//given formula
return((parseFloat(dso)*parseFloat(revenue))/365);
}


function Equity_calculate(lastyrequity, netprofitloss)
{
//given
return(parseFloat(lastyrequity)+parseFloat(netprofitloss));
}

function TPO_calculate(tradepayables, otherexpenses,materialexpenses)
{
//given
return((parseFloat(tradepayables)*365)/(parseFloat(otherexpenses)+parseFloat(materialexpenses)));
}

function trade_payables_calculate(tpo, otherexpenses,materialexpenses)
{
//given
return(((parseFloat(otherexpenses)+parseFloat(materialexpenses))*parseFloat(tpo))/365);
}

function DPO_calculate(tradepayables,materialexpenses,Other_Operating_Expenses)
{
//given
return((parseFloat(tradepayables)*365)/(parseFloat(materialexpenses)+parseFloat(Other_Operating_Expenses)));
}