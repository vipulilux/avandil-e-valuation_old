from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
db=SQLAlchemy()
app = Flask(__name__)
currentpath=os.getcwd()
print(currentpath)
currentpath=currentpath.replace('\\', '\\\\')
print(currentpath)
dbfilepath=currentpath+"\\\\database\\\\evaluationdatabase.db\\\\"
print("dbpath="+dbfilepath)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+dbfilepath
app.config['SECRET_KEY'] = "random string"
db.init_app(app)

# from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
#
# db=SQLAlchemy()
# app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/salescommission'
# app.config['SECRET_KEY'] = "random string"
# db.init_app(app)
from app import views

# currentpath=currentpath.replace('\\', '//')
# print(currentpath)
# dbfilepath=currentpath+"evaluationdatabase.db\\"
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+dbfilepath

