from flask import Flask, request, flash, url_for, redirect, render_template
from flask_sqlalchemy import SQLAlchemy
from app import db
from werkzeug.security import generate_password_hash, check_password_hash



class avanevaluation(db.Model):

    __tablename__ = 'avanevaluation'

    id = db.Column(db.Integer, primary_key=True)
    year = db.Column( db.String(100))
    emailaddress = db.Column(db.String(100))
    revenues = db.Column(db.String(100))
    materialexpenses = db.Column(db.String(100))
    personnelexpenses = db.Column(db.String(100))
    otherexpenses = db.Column(db.String(100))
    otherincome = db.Column(db.String(100))
    ebitda = db.Column(db.String(100))
    ebitdamargin = db.Column(db.String(100))
    depreciation = db.Column(db.String(100))
    ebit = db.Column(db.String(100))
    ebitmargin = db.Column(db.String(100))
    interestexpenses = db.Column(db.String(100))
    interestincome = db.Column(db.String(100))
    ebt = db.Column(db.String(100))
    incometax = db.Column(db.String(100))
    netincome = db.Column(db.String(100))
    fixedasset = db.Column(db.String(100))
    finanacialasset = db.Column(db.String(100))
    inventories = db.Column(db.String(100))
    trade = db.Column(db.String(100))
    othercurrentassets = db.Column(db.String(100))
    cash = db.Column(db.String(100))
    totalcurrentassets = db.Column(db.String(100))
    totalassets = db.Column(db.String(100))
    equity = db.Column(db.String(100))
    financialdebt = db.Column(db.String(100))
    pensionprovisions = db.Column(db.String(100))
    otherprovisions = db.Column(db.String(100))
    tradepayables = db.Column(db.String(100))
    otherliabilities = db.Column(db.String(100))
    totalliabilities = db.Column(db.String(100))
    totalequity = db.Column(db.String(100))
    capitalexpendituresinfixedasset = db.Column(db.String(100))
    capitalexpenditurepercent = db.Column(db.String(100))
    netcurrentassets = db.Column(db.String(100))
    netcurrentassetspercent = db.Column(db.String(100))


class user(db.Model):

    __tablename__='user'

    username = db.Column(db.String(100))
    useremail = db.Column(db.String(150),primary_key=True )
    password_hash= db.Column(db.String(100))
    varificationcode= db.Column(db.String(15))
    status = db.Column(db.String(10))


    @property
    def password(self):
        raise AttributeError("password not match")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __init__(self, company_name1, username1, password_hash1, varificationcode1,status1):
        print("getdata")
        self.username = company_name1
        self.useremail = username1
        self.password_hash = generate_password_hash(password_hash1)
        self.varificationcode = varificationcode1
        self.status=status1


class companyregistration(db.Model):

    __tablename__ = 'companyregistration'
    companyname = db.Column(db.String(100))
    address = db.Column(db.String(100))
    city = db.Column(db.String(100))
    state = db.Column(db.String(100))
    zip = db.Column(db.String(100))
    country = db.Column(db.String(100))
    website = db.Column(db.String(100))
    firstname = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    contactnumber = db.Column(db.String(100))
    emailaddress = db.Column(db.String(100), primary_key=True)

class Financial_Data(db.Model):
    __tablename__ = 'Financial_Data'
    id = db.Column(db.Integer, primary_key=True)
    emailaddress = db.Column(db.String(100))
    companyname = db.Column(db.String(100))
    year = db.Column( db.String(100))
    revenues = db.Column(db.String(100))
    materialexpenses = db.Column(db.String(100))
    personnelexpenses = db.Column(db.String(100))
    otherexpenses = db.Column(db.String(100))
    otherincome = db.Column(db.String(100))
    ebitda = db.Column(db.String(100))
    ebitdamargin = db.Column(db.String(100))
    depreciation = db.Column(db.String(100))
    ebit = db.Column(db.String(100))
    ebitmargin = db.Column(db.String(100))
    interestexpenses = db.Column(db.String(100))
    interestincome = db.Column(db.String(100))
    ebt = db.Column(db.String(100))
    incometax = db.Column(db.String(100))
    netincome = db.Column(db.String(100))
    fixedasset = db.Column(db.String(100))
    finanacialasset = db.Column(db.String(100))
    inventories = db.Column(db.String(100))
    trade = db.Column(db.String(100))
    othercurrentassets = db.Column(db.String(100))
    cash = db.Column(db.String(100))
    totalcurrentassets = db.Column(db.String(100))
    totalassets = db.Column(db.String(100))
    equity = db.Column(db.String(100))
    financialdebt = db.Column(db.String(100))
    pensionprovisions = db.Column(db.String(100))
    otherprovisions = db.Column(db.String(100))
    tradepayables = db.Column(db.String(100))
    otherliabilities = db.Column(db.String(100))
    totalliabilities = db.Column(db.String(100))
    totalequity = db.Column(db.String(100))
    capitalexpendituresinfixedasset = db.Column(db.String(100))
    capitalexpenditurepercent = db.Column(db.String(100))
    netcurrentassets = db.Column(db.String(100))
    netcurrentassetspercent = db.Column(db.String(100))
